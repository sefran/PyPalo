from pypalo.pypaloelement import PypaloElement

"""
File pypalocube.py
Cube Request Class for Palo Jedox MOLAP Database
Contains Cube Class Palo API

@author Franc SERRES aka sefran
@version 0.5
@licence GPL V3
"""
class PypaloCube(PypaloElement):
    """
    Private Constructor Method PypaloCube('Host', 'Port')
    PypaloCube = Object pointer of Configuration

    usage:
    ~~~~~~~~~~~~~{.py}
    from pypalo.pypalocube import PypaloCube

    palocube = PypaloCube('127.0.0.1', '7777')
    if palocube.login('admin', 'admin'):
        print('Connection Serveur OK')
        if palocube.getdatabases():
            databases = palocube.response
            if palocube.getdimensionsdatabase(databases[0]):
                dimensions = palocube.response
                "Put your featured code here"
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        if palocube.logout():
            print('logout OK')
        else:
            print(palocube.errors)
    else:
        print(palocube.errors)
    ~~~~~~~~~~~~~
    """
    def __init__(self, host, port):
        """
        Parameters :
        host = Ip adress of host server palo like '127.0.0.1'
        port = Number of port host server palo like '7777'
        """
        PypaloElement.__init__(self, host, port)

    def clearcube(self, databasecubeclear, cubeclear, areaclear=None):
        """
        Method clearcube()

        Clears a cube

        databasecubeclear = Dictionary of a database
        cubeclear = Dictionary of a cube
        areaclear = List of list of dictionary elements or dictionary element\
 of dimension like :
        listcubeelementdimension[
                listelementdimension0[
                                    dictionaryelement{...
                                                    'id': Identifiant element
                                                     }
                                     ],
                dictionaryelementdimension1{...
                                            'id': Identifiant element
                                           }
                                ]

        Return true if lock cube area request OK
        Property response return dictionary cube like :
                dictionary : {'id': Identifier of the cube,
                              'name': Name of the cube,
                              'number_dimensions': Number of dimensions,
                              'dimensions': List of dimension dictionary,
                              'number_cells': Total number of cell,
                              'number_filled_cells': Number of filled numeric \
base cells plus number of filled string cells,
                              'status': Status of cube (0=unloaded, 1=loaded \
and 2=changed),
                              'type': Type of cube (0=Normal, 1=System, \
2=Attribute, 3=User Info, 4=Gpu Type),
                              'cube_token': The cube token of the cube
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/clear'
        if areaclear:
            if len(areaclear) != int(cubeclear['number_dimensions']):
                self.response = None
                self.errors += 'Palo Error clear area of cube return : '
                self.errors += 'Number of elements list parameters area not \
égual number of dimension cube'
                return False
            area = ''
            for areaelement in areaclear:
                if type(areaelement) is list:
                    for elarea in areaelement:
                        area += elarea['id'] + ':'
                    area = area[:-1] + ','
                else:
                    area += areaelement['id'] + ','
            area = area[:-1]
            self.param = {'database': databasecubeclear['id'],
                          'cube': cubeclear['id'],
                          'area': area,
                          'sid': self.keydw
                         }
        else:
            self.param = {'database': databasecubeclear['id'],
                          'cube': cubeclear['id'],
                          'complete': '1',
                          'sid': self.keydw
                         }
        if self.urlresult:
            responseclearcube = self.getdata()
            clearcubedict = {"id":None,
                             "name":None,
                             "number_dimensions":None,
                             "dimensions":None,
                             "number_cells":None,
                             "number_filled_cells":None,
                             "status":None,
                             "type":None,
                             "cube_token":None,
                            }
            responseclearcube = responseclearcube.split(';')[:-1]
            clearcubedict["id"] = responseclearcube[0]
            clearcubedict["name"] = responseclearcube[1]
            clearcubedict["number_dimensions"] = responseclearcube[2]
            listdimcube = responseclearcube[3].split(',')
            dimscubeclear = []
            for dimcubeclear in listdimcube:
                self.infodimension(databasecubeclear, {'id': dimcubeclear})
                dimscubeclear.append(self.response)
            clearcubedict["dimensions"] = dimscubeclear.copy()
            clearcubedict["number_cells"] = responseclearcube[4]
            clearcubedict["number_filled_cells"] = responseclearcube[5]
            clearcubedict["status"] = responseclearcube[6]
            clearcubedict["type"] = responseclearcube[7]
            clearcubedict["cube_token"] = responseclearcube[8]
            self.response = clearcubedict.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error clear cube return : '
            self.errors += str(self.getdata())
            return False

    def commitcube(self, databasecubecommit, cubecommit, lockcommit):
        """
        Method commitcube()

        Commits changes of a locked cube area

        databasecubecommit = Dictionary of a database
        cubecommit = Dictionary of a cube
        lockcommit = Dictionary of a lock

        Return true if commit cube request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.lockcube(testdatabase, testpalocube, [ \
{'id': '0'}, [{'id': '0'}, {'id': '1'}, {'id': '2'}]]):
                            palocubelock = palocube.response
                            print('Lock', palocubelock)
                        else:
                            print(palocube.errors)
                        if palocube.commitcube(testdatabase, testpalocube, \
palocubelock):
                            print('Commit palodatabase OK')
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/commit'
        self.param = {'database': databasecubecommit['id'],
                      'cube': cubecommit['id'],
                      'lock': lockcommit['id'],
                      'sid': self.keydw
                             }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error commit cube return : '
            self.errors += str(self.getdata())
            return False

    def createcube(self, databasecube, newnamecube, dimensionscube, 
                                                                dimtypecube):
        """
        Method createcube()

        Creates a cube

        databasecube = Dictionary of a database
        newnamecube = Name of the new cube
        dimensionscube = List of dictionary dimension
        dimtypecube = Type of the dimention (1=Normal, 3=User Info)

        Return true if create cube request OK
        Property response return dictionary cube like :
                dictionary : {'id': Identifier of the cube,
                              'name': Name of the cube,
                              'number_dimensions': Number of dimensions,
                              'dimensions': List of dimension dictionary,
                              'number_cells': Total number of cell,
                              'number_filled_cells': Number of filled numeric \
base cells plus number of filled string cells,
                              'status': Status of cube (0=unloaded, 1=loaded \
and 2=changed),
                              'type': Type of cube (0=Normal, 1=System, \
2=Attribute, 3=User Info, 4=Gpu Type),
                              'cube_token': The cube token of the cube
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/create'
        listdimensions = ''
        for dimension in dimensionscube:
            listdimensions += dimension['id'] + ','
        self.param = {'database': databasecube['id'],
                      'new_name': newnamecube,
                      'dimensions': listdimensions[:-1],
                      'type': dimtypecube,
                      'sid': self.keydw
                             }
        if self.urlresult:
            responsecreatecube = self.getdata()
            cubedict = {"id":None,
                        "name":None,
                        "number_dimensions":None,
                        "dimensions":None,
                        "number_cells":None,
                        "number_filled_cells":None,
                        "status":None,
                        "type":None,
                        "cube_token":None,
                       }
            responsecreatecube = responsecreatecube.split(';')[:-1]
            cubedict["id"] = responsecreatecube[0]
            cubedict["name"] = responsecreatecube[1]
            cubedict["number_dimensions"] = responsecreatecube[2]
            listdimcube = responsecreatecube[3].split(',')
            dimscubecreate = []
            for dimcubecreate in listdimcube:
                self.infodimension(databasecube, {'id': dimcubecreate})
                dimscubecreate.append(self.response)
            cubedict["dimensions"] = dimscubecreate.copy()
            cubedict["number_cells"] = responsecreatecube[4]
            cubedict["number_filled_cells"] = responsecreatecube[5]
            cubedict["status"] = responsecreatecube[6]
            cubedict["type"] = responsecreatecube[7]
            cubedict["cube_token"] = responsecreatecube[8]
            self.response = cubedict.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Create cube return : '
            self.errors += str(self.getdata())
            return False

    def destroycube(self, databasecubedestroy, cubedestroy):
        """
        Method destroycube()

        Deletes cube

        databasecubedestroy = Dictionary of a database
        cubedestroy = Dictionary of a cube

        Return true if create cube request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/destroy'
        self.param = {'database': databasecubedestroy['id'],
                      'cube': cubedestroy['id'],
                      'sid': self.keydw
                             }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error destroy cube return : '
            self.errors += str(self.getdata())
            return False

    def infocube(self, databasecubeinfo, cubeinfo):
        """
        Method infocube()

        Shows cube data

        databasecubeinfo = Dictionary of a database
        cubeinfo = Dictionary of a cube

        Return true if info cube request OK
        Property response return dictionary cube like :
                dictionary : {'id': Identifier of the cube,
                              'name': Name of the cube,
                              'number_dimensions': Number of dimensions,
                              'dimensions': List of dictionary dimension,
                              'number_cells': Total number of cell,
                              'number_filled_cells': Number of filled numeric \
base cells plus number of filled string cells,
                              'status': Status of cube (0=unloaded, 1=loaded \
and 2=changed),
                              'type': Type of cube (0=Normal, 1=System, \
2=Attribute, 3=User Info, 4=Gpu Type),
                              'cube_token': The cube token of the cube
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.infocube(testdatabase, testpalocube):
                            print('Info cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/info'
        self.param = {'database': databasecubeinfo['id'],
                      'cube': cubeinfo['id'],
                      'sid': self.keydw
                             }
        if self.urlresult:
            responseinfocube = self.getdata()
            cubedict = {"id":None,
                        "name":None,
                        "number_dimensions":None,
                        "dimensions":None,
                        "number_cells":None,
                        "number_filled_cells":None,
                        "status":None,
                        "type":None,
                        "cube_token":None,
                       }
            responseinfocube = responseinfocube.split(';')[:-1]
            cubedict["id"] = responseinfocube[0]
            cubedict["name"] = responseinfocube[1]
            cubedict["number_dimensions"] = responseinfocube[2]
            listdimcubeinfo = responseinfocube[3].split(',')
            dimscubeinfo = []
            for dimcubeinfo in listdimcubeinfo:
                self.infodimension(databasecubeinfo, {'id': dimcubeinfo})
                dimscubeinfo.append(self.response)
            cubedict["dimensions"] = dimscubeinfo.copy()
            cubedict["number_cells"] = responseinfocube[4]
            cubedict["number_filled_cells"] = responseinfocube[5]
            cubedict["status"] = responseinfocube[6]
            cubedict["type"] = responseinfocube[7]
            cubedict["cube_token"] = responseinfocube[8]
            self.response = cubedict.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error info cube return : '
            self.errors += str(self.getdata())
            return False

    def loadcube(self, databasecubeload, cubeload):
        """
        Method loadcube()

        Loads cube

        databasecubeload = Dictionary of a database
        cubeload = Dictionary of a cube

        Return true if load cube request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.savecube(testdatabase, testpalocube):
                            print('Save cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.unloadcube(testdatabase, testpalocube):
                            print('Unload cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.infocube(testdatabase, testpalocube):
                            print('Info cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.loadcube(testdatabase, testpalocube):
                            print('Load cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/load'
        self.param = {'database': databasecubeload['id'],
                      'cube': cubeload['id'],
                      'sid': self.keydw
                             }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error load cube return : '
            self.errors += str(self.getdata())
            return False

    def lockcube(self, databasecubelock, cubelock, arealock):
        """
        Method lockcube()

        Locks a cube area of elements (first rang of elements is\
 first dimention of cube)

        databasecubelock = Dictionary of a database
        cubelock = Dictionary of a cube
        arealock = List of list of dictionary elements or dictionary element\
 of dimension like :
        listcubeelementdimension[
                listelementdimension0[
                                    dictionaryelement{...
                                                    'id': Identifiant element
                                                     }
                                     ],
                dictionaryelementdimension1{...
                                            'id': Identifiant element
                                           }
                                ]

        Return true if lock cube area request OK
        Property response return dictionary lock like :
                dictionary : {'id': Identifier of the lock,
                              'area': List of identifier element or of list of \
element identifier),
                              '': ,
                              '': ,
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.lockcube(testdatabase, testpalocube, [\
{'id': '0'}, [{'id': '0'}, {'id': '1'}, {'id': '2'}]]):
                            print('Lock', palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        if len(arealock) != int(cubelock['number_dimensions']):
            self.response = None
            self.errors += 'Palo Error lock area of cube return : '
            self.errors += 'Number of elements list parameters area not égual \
number of dimension cube'
            return False
        area = ''
        for areaelement in arealock:
            if type(areaelement) is list:
                for elarea in areaelement:
                    area += elarea['id'] + ':'
                area = area[:-1] + ','
            else:
                area += areaelement['id'] + ','
        area = area[:-1]
        self.cmd = 'cube/lock'
        self.param = {'database': databasecubelock['id'],
                      'cube': cubelock['id'],
                      'area': area,
                      'sid': self.keydw
                     }
        if self.urlresult:
            responselockareacube = self.getdata()
            lockdict = {"id":None,
                        "area":None,
                        "user":None,
                        "steps":None,
                       }
            responselockareacube = responselockareacube.split(';')[:-1]
            lockdict["id"] = responselockareacube[0]
            listlockareacube = []
            indexdimension = 0
            for listelarea in responselockareacube[1].split(','):
                resultarea = listelarea.split(':')
                if len(resultarea) > 1:
                    lockareacube = []
                    for ellistarea in resultarea:
                        self.getelementdimension(databasecubelock, \
cubelock['dimensions'][indexdimension], ellistarea)
                        lockareacube.append(self.response)
                else:
                    self.getelementdimension(databasecubelock, \
cubelock['dimensions'][indexdimension], resultarea[0])
                    lockareacube = self.response
                listlockareacube.append(lockareacube)
                indexdimension += 1
            lockdict["area"] = listlockareacube 
            lockdict["user"] = responselockareacube[2]
            lockdict["steps"] = responselockareacube[3]
            self.response = lockdict.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error lock area of cube return : '
            self.errors += str(self.getdata())
            return False

    def getlockcube(self, databasecubelistlocks, cubelistlocks):
        """
        Method getlockcube()

        List the locked cube area

        databasecubelock = Dictionary of a database
        cubelock = Dictionary of a cube

        Return true if get lock list cube request OK
        Property response return dictionary lock like :
                dictionary : {'id': Identifier of the lock,
                              'area': List of identifier element or of list of \
element identifier),
                              '': ,
                              '': ,
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.lockcube(testdatabase, testpalocube, [\
{'id': '0'}, [{'id': '0'}, {'id': '1'}, {'id': '2'}]]):
                            palocubelock = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.getlockcube(testdatabase, testpalocube):
                            print('Lock', palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/locks'
        self.param = {'database': databasecubelistlocks['id'],
                      'cube': cubelistlocks['id'],
                      'sid': self.keydw
                     }
        if self.urlresult:
            responselocksareacube = self.getdata()
            locksdict = {"id":None,
                        "area":None,
                        "user":None,
                        "steps":None,
                       }
            responselocksareacube = \
                                responselocksareacube.split(';')[:-1]
            if len(responselocksareacube) == 0:
                self.response = None
                self.errors += 'Palo Error getlockcube return : '
                self.errors += 'None lock for cube area'
                return False
            locksdict["id"] = responselocksareacube[0]
            listlocksareacube = []
            indexdimensionl = 0
            for listelareal in responselocksareacube[1].split(','):
                resultareal = listelareal.split(':')
                if len(resultareal) > 1:
                    locksareacube = []
                    for ellistareal in resultareal:
                        self.getelementdimension(databasecubelistlocks, \
cubelistlocks['dimensions'][indexdimensionl], ellistareal)
                        locksareacube.append(self.response)
                else:
                    self.getelementdimension(databasecubelistlocks, \
cubelistlocks['dimensions'][indexdimensionl], resultareal[0])
                    locksareacube = self.response
                listlocksareacube.append(locksareacube)
                indexdimensionl += 1
            locksdict["area"] = listlocksareacube 
            locksdict["user"] = responselocksareacube[2]
            locksdict["steps"] = responselocksareacube[3]
            self.response = locksdict.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error getlockcube return : '
            self.errors += str(self.getdata())
            return False

    def renamecube(self, databasecuberename, cuberename, newrenamecube):
        """
        Method renamecube()

        Renames a cube

        databasecuberename = Dictionary of a database
        cuberename = Dictionary of a cube
        newrenamecube = New name of the cube

        Return true if rename cube request OK
        Property response return dictionary cube like :
                dictionary : {'id': Identifier of the cube,
                              'name': Name of the cube,
                              'number_dimensions': Number of dimensions,
                              'dimensions': List of dimension identifiers,
                              'number_cells': Total number of cell,
                              'number_filled_cells': Number of filled numeric \
base cells plus number of filled string cells,
                              'status': Status of cube (0=unloaded, 1=loaded \
and 2=changed),
                              'type': Type of cube (0=Normal, 1=System, \
2=Attribute, 3=User Info, 4=Gpu Type),
                              'cube_token': The cube token of the cube
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.renamecube(testdatabase, testpalocube, \
'NewCube'):
                            print('Rename cube {0} : \
'.format(testpalocube['name']), palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/rename'
        self.param = {'database': databasecuberename['id'],
                      'cube': cuberename['id'],
                      'new_name': newrenamecube,
                      'sid': self.keydw
                             }
        if self.urlresult:
            responserenamecube = self.getdata()
            cubedict = {"id":None,
                        "name":None,
                        "number_dimensions":None,
                        "dimensions":None,
                        "number_cells":None,
                        "number_filled_cells":None,
                        "status":None,
                        "type":None,
                        "cube_token":None,
                       }
            responserenamecube = responserenamecube.split(';')[:-1]
            cubedict["id"] = responserenamecube[0]
            cubedict["name"] = responserenamecube[1]
            cubedict["number_dimensions"] = responserenamecube[2]
            listdimcuberename = responserenamecube[3].split(',')
            dimscuberename = []
            for dimcuberename in listdimcuberename:
                self.infodimension(databasecuberename, {'id': dimcuberename})
                dimscuberename.append(self.response)
            cubedict["dimensions"] = dimscuberename.copy()
            cubedict["number_cells"] = responserenamecube[4]
            cubedict["number_filled_cells"] = responserenamecube[5]
            cubedict["status"] = responserenamecube[6]
            cubedict["type"] = responserenamecube[7]
            cubedict["cube_token"] = responserenamecube[8]
            self.response = cubedict.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error rename cube return : '
            self.errors += str(self.getdata())
            return False

    def rollbackcube(self, databasecuberollback, cuberollback, lockrollback,
                                                            stepsrollback=None):
        """
        Method rollbackcube()

        Roolback changes of a locked cube area

        databasecuberollback = Dictionary of a database
        cuberollback = Dictionary of a cube
        lockrollback = Dictionary of a lock
        stepsrollback = Number of steps to rollback (an empty value means undo\
 all steps and remove lock)

        Return true if rollback cube request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.lockcube(testdatabase, testpalocube, [\
{'id': '0'}, [{'id': '0'}, {'id': '1'}, {'id': '2'}]]):
                            print('Lock', palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.getlockcube(testdatabase, testpalocube):
                            palocubelock = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.rollbackcube(testdatabase, testpalocube, \
palocubelock):
                            print('Roolback cube OK')
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/rollback'
        if stepsrollback:
            self.param = {'database': databasecuberollback['id'],
                          'cube': cuberollback['id'],
                          'lock': lockrollback['id'],
                          'steps': stepsrollback,
                          'sid': self.keydw
                             }
        else:
            self.param = {'database': databasecuberollback['id'],
                          'cube': cuberollback['id'],
                          'lock': lockrollback['id'],
                          'sid': self.keydw
                             }            
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error rollback cube return : '
            self.errors += str(self.getdata())
            return False

    def getrulescube(self, databasecuberules, cuberules, useidentifier=None):
        """
        Method getrulescube()

        List the rules for a cube

        databasecuberules = Dictionary of a database
        cuberules = Dictionary of a cube
        useidentifier = Use identifier in textual representation of the rule

        Return true if save cube request OK
        Property response return dictionary rule like :
                dictionary : {'id': Identifier of the rule,
                              'rule_string': Textual representation for the \
rule,
                              'external_identifier': External identifier of the 
\ rule,
                              'comment': comment for the rule,
                              'timestamp': creation time of the rule in \
seconds since 01-01-1970,
                              'active': 0=rule is not active, 1=rule is active,
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.getrulescube(testdatabase, testpalocube):
                            print('Rules of cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/rules'
        if useidentifier:
            self.param = {'database': databasecuberules['id'],
                          'cube': cuberules['id'],
                          'use_identifier': '1',
                          'sid': self.keydw
                         }
        else:
            self.param = {'database': databasecuberules['id'],
                          'cube': cuberules['id'],
                          'sid': self.keydw
                         }
        if self.urlresult:
            responserulescube = self.getdata()
            ruledict = {"id":None,
                        "rule_string":None,
                        "external_identifier":None,
                        "comment":None,
                        "timestamp":None,
                        "active":None,
                       }
            if responserulescube:
                responserulescube = responserulescube.split(';')[:-1]
                ruledict["id"] = responserulescube[0]
                ruledict["rule_string"] = responserulescube[1]
                ruledict["external_identifier"] = responserulescube[2]
                ruledict["comment"] = responserulescube[3].split(',')
                ruledict["timestamp"] = responserulescube[4]
                ruledict["active"] = responserulescube[5]
                self.response = ruledict.copy()
                self.errors = None
                return True
            else:
                self.response = ruledict.copy()
                self.errors = None
                return True
        else:
            self.response = None
            self.errors += 'Palo Error rules fo cube return : '
            self.errors += str(self.getdata())
            return False

    def savecube(self, databasecubesave, cubesave):
        """
        Method savecube()

        Saves cube data to disk

        databasecubesave = Dictionary of a database
        cubesave = Dictionary of a cube

        Return true if save cube request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.savecube(testdatabase, testpalocube):
                            print('Save cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/save'
        self.param = {'database': databasecubesave['id'],
                      'cube': cubesave['id'],
                      'sid': self.keydw
                             }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error save cube return : '
            self.errors += str(self.getdata())
            return False

    def unloadcube(self, databasecubeunload, cubeunload):
        """
        Method unloadcube()

        Unloads cube from memory

        databasecubeunload = Dictionary of a database
        cubeunload = Dictionary of a cube

        Return true if unload cube request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocube import PypaloCube

        palocube = PypaloCube('127.0.0.1', '7777')
        if palocube.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocube.getdatabases():
                databases = palocube.response
                if palocube.getdimensionsdatabase(databases[0]):
                    dimensions = palocube.response
                    if palocube.createdatabase("test"):
                        testdatabase = palocube.response
                        if palocube.infodatabase(testdatabase):
                            print('Info test database : ', \
palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocube.response)
                            dimension0 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocube.response)
                            dimension1 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocube.response)
                            dimension2 = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocube.response)
                            testpalocube = palocube.response
                        else:
                            print(palocube.errors)
                        if palocube.savecube(testdatabase, testpalocube):
                            print('Save cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.unloadcube(testdatabase, testpalocube):
                            print('Unload cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.infocube(testdatabase, testpalocube):
                            print('Info cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocube.errors)
                        if palocube.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocube.errors)
                        if palocube.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
            if palocube.logout():
                print('logout OK')
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cube/unload'
        self.param = {'database': databasecubeunload['id'],
                      'cube': cubeunload['id'],
                      'sid': self.keydw
                             }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error unload cube return : '
            self.errors += str(self.getdata())
            return False


################################################################################

if __name__ == "__main__":
    palocube = PypaloCube('127.0.0.1', '7777')
    if palocube.login('admin', 'admin'):
        print('Connection Serveur OK')
        if palocube.getdatabases():
            palodatabases = palocube.response
            if palocube.getdimensionsdatabase(palodatabases[0]):
                palocubes = palocube.response
                if palocube.createdatabase("test"):
                    print('Database test created')
                    testdatabase = palocube.response
                    if palocube.infodatabase(testdatabase):
                        print('Info test database : ', palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.createdimension(testdatabase, 'testdim0'):
                        print('Creating dimension testdim0 : ', \
                                                        palocube.response)
                        testdimension0 = palocube.response
                        if palocube.createelementbulk(testdatabase, \
                        testdimension0, ['el0', 'el1', 'el2', 'el3'], '1'):
                            print('el0, el1, el2 and el3 created')
                        else:
                            print(palocube.errors)
                        if palocube.getelementsdimension(testdatabase,
                                                                testdimension0):
                            print('Elements of database {0} and dimension \
{1}'.format(testdatabase["name"], testdimension0["name"]))
                            print(palocube.response)
                            elementsdimension0 = palocube.response
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                    if palocube.createdimension(testdatabase, 'testdim1'):
                        print('Creating dimension testdim1 : ', \
                                                        palocube.response)
                        testdimension1 = palocube.response
                        if palocube.createelementbulk(testdatabase, \
                        testdimension1, ['a0', 'a1', 'a2', 'a3', 'a4'], '1'):
                            print('a0, a1, a2, a3 and a4 created')
                        else:
                            print(palocube.errors)
                        if palocube.getelementsdimension(testdatabase,
                                                                testdimension1):
                            print('Elements of database {0} and dimension \
{1}'.format(testdatabase["name"], testdimension1["name"]))
                            print(palocube.response)
                            elementsdimension1 = palocube.response
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                    if palocube.createdimension(testdatabase, 'testdim2'):
                        print('Creating dimension testdim2 : ', \
                                                        palocube.response)
                        testdimension2 = palocube.response
                        if palocube.createelementbulk(testdatabase, \
                        testdimension2, ['b0', 'b1', 'b2'], '1'):
                            print('b0, b1 and b2 created')
                        else:
                            print(palocube.errors)
                        if palocube.getelementsdimension(testdatabase,
                                                                testdimension2):
                            print('Elements of database {0} and dimension \
{1}'.format(testdatabase["name"], testdimension2["name"]))
                            print(palocube.response)
                            elementsdimension2 = palocube.response
                        else:
                            print(palocube.errors)
                    else:
                        print(palocube.errors)
                    if palocube.createcube(testdatabase, 'testcube', 
[testdimension1, testdimension2], '0'):
                        print('Cube created : ', palocube.response)
                        testpalocube = palocube.response
                    else:
                        print(palocube.errors)
                    if palocube.getrulescube(testdatabase, testpalocube):
                        print('Rules of the cube {0} : \
'.format(testpalocube['name']), palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.renamecube(testdatabase, testpalocube,\
                                                                    'NewCube'):
                        print('Rename cube {0} : '.format(testpalocube['name']),
                                                            palocube.response)
                        testpalocube = palocube.response
                    else:
                        print(palocube.errors)
                    if palocube.savecube(testdatabase, testpalocube):
                        print('Save cube {0} : '.format(testpalocube['name']),
                                                            palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.unloadcube(testdatabase, testpalocube):
                        print('Unload cube {0} : \
'.format(testpalocube['name']), palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.infocube(testdatabase, testpalocube):
                        print('Info cube {0} : '.format(testpalocube['name']), \
                                                            palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.loadcube(testdatabase, testpalocube):
                        print('Load cube {0} : '.format(testpalocube['name']), \
                                                            palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.lockcube(testdatabase, testpalocube, [{'id':
'0'}, [{'id': '0'}, {'id': '1'}, {'id': '2'}]]):
                        print('Lock', palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.getlockcube(testdatabase, testpalocube):
                        palocubelock = palocube.response
                    else:
                        print(palocube.errors)
                    if palocube.rollbackcube(testdatabase, testpalocube, \
                                                                palocubelock):
                        print('Roolback cube OK')
                    else:
                        print(palocube.errors)
                    if palocube.commitcube(testdatabase, testpalocube, \
                                                                palocubelock):
                        print('Commit palodatabase OK')
                    else:
                        print(palocube.errors)
                    if palocube.clearcube(testdatabase, testpalocube):
                        print('Clear cube : ', palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.destroycube(testdatabase, testpalocube):
                        print('Destroy cube {0} : \
'.format(testpalocube['name']), palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.destroydimension(testdatabase, \
                                                                testdimension0):
                        print('Dimension testdim deleted : ', \
                                                        palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.destroydimension(testdatabase, \
                                                                testdimension1):
                        print('Dimension testdim deleted : ', \
                                                        palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.destroydimension(testdatabase, \
                                                                testdimension2):
                        print('Dimension testdim deleted : ', \
                                                        palocube.response)
                    else:
                        print(palocube.errors)
                    if palocube.destroydatabase(testdatabase):
                        print('Database test deleted : ', \
                                                        palocube.response)
                    else:
                        print(palocube.errors)
                else:
                    print(palocube.errors)
            else:
                print(palocube.errors)
        else:
            print(palocube.errors)
        if palocube.logout():
            print('logout OK')
        else:
            print(palocube.errors)
    else:
        print(palocube.errors)
