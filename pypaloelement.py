from pypalo.pypalodimension import PypaloDimension

"""
File pypaloelement.py
Element Request Class for Palo Jedox MOLAP Database
Contains Server Class Palo API

@author Franc SERRES aka sefran
@version 0.5
@licence GPL V3
"""
class PypaloElement(PypaloDimension):
    """
    Private Constructor Method PypaloElement('Host', 'Port')
    PyPaloElement = Object pointer of Configuration

    usage:
    ~~~~~~~~~~~~~{.py}
    from pypalo.pypaloelement import PypaloElement

    paloelement = PypaloElement('127.0.0.1', '7777')
    if paloelement.login('admin', 'admin'):
        print('Connection Serveur OK')
        if paloelement.getdatabases():
            databases = paloelement.response
            if paloelement.getdimensionsdatabase(databases[0]):
                dimensions = paloelement.response
                "Put your featured code here"
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        if paloelement.logout():
            print('logout OK')
        else:
            print(paloelement.errors)
    else:
        print(paloelement.errors)
    ~~~~~~~~~~~~~
    """
    def __init__(self, host, port):
        """
        Parameters :
        host = Ip adress of host server palo like '127.0.0.1'
        port = Number of port host server palo like '7777'
        """
        PypaloDimension.__init__(self, host, port)

    def appendelement(self, appenddatabase, appenddimension, appendelement, \
                                appendlistchildren, appendlistweights=None):
        """
        Method appendelement()

        Updates the children of an consolidated element

        appenddatabase = Dictionary of a database
        appenddimension = Dictionary of dimension
        appendelement = Dictionary of element
        appendlistchildren = List of children identifiers to append
        appendlistweights = List of children weights (defaults weights=1)

        Return true if append element request OK
        Property response return dictionary element like :
                dictionary : {'id': Identifier of the element,
                              'name': Name of the element,
                              'position': Number of dimension in the database,
                              'level': Level of the element,
                              'indent': Indent of the element,
                              'depth': Depth of the element,
                              'type': Type of element (1=Numeric, 2=String, \
4=Consolidated),
                              'number_parents': Number of parents,
                              'parents': list of parent identifiers,
                              'number_children': Number of children,
                              'children': list of children identifiers,
                              'weights': list of children weight
                             }

        usage:
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelement(testdatabase, \
testdimension, 'elements', '4'):
                                testelement = paloelement.response
                                print('Test elements :', testelement)
                            else:
                                print(paloelement.errors)
                            if paloelement.createelement(testdatabase, \
testdimension, 'el1', '1'):
                                test2element = paloelement.response
                                print('Test element :', test2element)
                            else:
                                print(paloelement.errors)
                            if paloelement.appendelement(testdatabase, \
testdimension, testelement, [test2element["id"]]):
                            else:
                                print(paloelement.errors)
                            if paloelement.infoelement(testdatabase, \
testdimension, testelement):
                                print('Append test2element :', \
paloelement.response)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'element/append'
        if appendelement["type"] == '4':
            if appendlistweights:
                self.param = {'database': appenddatabase["id"],
                              'dimension': appenddimension["id"],
                              'element': appendelement["id"],
                              'children': \
            str(appendlistchildren).replace('\'', '').replace(' ', '')[1:-1],
                              'weights': \
            str(appendlistweights).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
            else:
                self.param = {'database': appenddatabase["id"],
                              'dimension': appenddimension["id"],
                              'element': appendelement["id"],
                              'children': \
            str(appendlistchildren).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
        else:
            self.response = None
            self.errors += 'Palo Error append the Element '
            self.errors += appendelement["name"]
            self.errors += ' type is not Consolidated'
            return False

        if self.urlresult:
            responseappend = self.getdata()
            listelementappend = {"id":None,
                                 "name":None,
                                 "position":None,
                                 "level":None,
                                 "indent":None,
                                 "depth":None,
                                 "type":None,
                                 "number_parents":None,
                                 "parents":None,
                                 "number_children":None,
                                 "children":None,
                                 "weights":None,
                                }
            responseappend = responseappend.split(';')[:-1]
            listelementappend["id"] = responseappend[0]
            listelementappend["name"] = responseappend[1]
            listelementappend["position"] = responseappend[2]
            listelementappend["level"] = responseappend[3]
            listelementappend["indent"] = responseappend[4]
            listelementappend["depth"] = responseappend[5]
            listelementappend["type"] = responseappend[6]
            listelementappend["number_parents"] = responseappend[7]
            listelementappend["parents"] = responseappend[8].split(',')
            listelementappend["number_children"] = responseappend[9]
            listelementappend["children"] = responseappend[10].split(',')
            listelementappend["weights"] = responseappend[11].split(',')
            self.response = listelementappend.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Append Element return : '
            self.errors += str(self.getdata())
            return False

    def createelement(self, database, dimension, newelementname, eltype, \
                                        childrenlist=None, weightslist=None):
        """
        Method createelement()

        Creates an element

        database = Dictionary of a database
        dimension = Dictionary of dimension
        newelementname = Name of the new element
        type = Type of the element (1=Numeric, 2=String, 4=Consolidated)
        childrenlist = list of children identifiers (only for type=4)
        weightslist = list of children weight (Only for type 4 with defaults to\
 weight=1 to each child)

        Return true if create element request OK
        Property response return dictionary element like :
                dictionary : {'id': Identifier of the element,
                              'name': Name of the element,
                              'position': Number of dimension in the database,
                              'level': Level of the element,
                              'indent': Indent of the element,
                              'depth': Depth of the element,
                              'type': Type of element (1=Numeric, 2=String, \
4=Consolidated),
                              'number_parents': Number of parents,
                              'parents': list of parent identifiers,
                              'number_children': Number of children,
                              'children': list of children identifiers,
                              'weights': list of children weight
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelement(testdatabase, \
testdimension, 'element', '1'):
                                testelement = paloelement.response
                                print('Test element :', testelement)
                            else:
                                print(paloelement.errors)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'element/create'
        if eltype == '4':
            if weightslist:
                self.param = {'database': database["id"],
                              'dimension': dimension["id"],
                              'new_name': newelementname,
                              'type': eltype,
                              'children': \
                    str(childrenlist).replace('\'', '').replace(' ', '')[1:-1],
                              'weights' : \
                    str(weightslist).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
            else:
                self.param = {'database': database["id"],
                              'dimension': dimension["id"],
                              'new_name': newelementname,
                              'type': eltype,
                              'children': \
                    str(childrenlist).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
        else:
            self.param = {'database': database["id"],
                          'dimension': dimension["id"],
                          'new_name': newelementname,
                          'type': eltype,
                          'sid': self.keydw
                         }
        if self.urlresult:
            response = self.getdata()
            listelement = {"id":None,
                           "name":None,
                           "position":None,
                           "level":None,
                           "indent":None,
                           "depth":None,
                           "type":None,
                           "number_parents":None,
                           "parents":None,
                           "number_children":None,
                           "children":None,
                           "weights":None,
                          }
            response = response.split(';')[:-1]
            listelement["id"] = response[0]
            listelement["name"] = response[1]
            listelement["position"] = response[2]
            listelement["level"] = response[3]
            listelement["indent"] = response[4]
            listelement["depth"] = response[5]
            listelement["type"] = response[6]
            listelement["number_parents"] = response[7]
            listelement["parents"] = response[8].split(',')
            listelement["number_children"] = response[9]
            listelement["children"] = response[10].split(',')
            listelement["weights"] = response[11].split(',')
            self.response = listelement.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Create Element return : '
            self.errors += str(self.getdata())
            return False

    def createelementbulk(self, database, dimension, listnewelementname, \
eltype, childrenlist=None, weightslist=None):
        """
        Method createelementbulk()

        Creates multiple elements of the same type

        database = Dictionary of a database
        dimension = Dictionary of dimension
        listnewelementname = List of name of the new element
        type = Type of the element (1=Numeric, 2=String, 4=Consolidated)
        childrenlist = list of children identifiers (only for type=4)
        weightslist = list of children weight (Only for type 4 with defaults to\
 weight=1 to each child)

        Return true if destroy bulk element request OK
        Property response return '1' for OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelementbulk(testdatabase, \
testdimension, ['element1', 'element2', 'element3'], '1'):
                                print('element1, element2, element3 created')
                            else:
                                print(paloelement.errors)
                            if paloelement.getelementsdimension(testdatabase, \
testdimension):
                                print('Elements of database {0} and dimension \
{1} : '.format(testdatabase["name"], testdimension["name"]), \
paloelement.response)
                            else:
                                print(paloelement.errors)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'element/create_bulk'
        if eltype == '4':
            if weightslist:
                self.param = {'database': database["id"],
                              'dimension': dimension["id"],
                              'name_elements': \
            str(listnewelementname).replace('\'', '').replace(' ', '')[1:-1],
                              'types': eltype,
                              'children': \
                str(childrenlist).replace('\'', '').replace(' ', '')[1:-1],
                              'weights' : \
                    str(weightslist).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
            else:
                self.param = {'database': database["id"],
                              'dimension': dimension["id"],
                              'name_elements': \
            str(listnewelementname).replace('\'', '').replace(' ', '')[1:-1],
                              'types': eltype,
                              'children': \
                str(childrenlist).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
        else:
            self.param = {'database': database["id"],
                          'dimension': dimension["id"],
                          'name_elements': \
            str(listnewelementname).replace('\'', '').replace(' ', '')[1:-1],
                          'type': eltype,
                          'sid': self.keydw
                         }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Create bulk Element return : '
            self.errors += str(self.getdata())
            return False

    def destroyelement(self, databasedestroy, dimensiondestroy, destroyelement):
        """
        Method destroyelement()

        Deletes an element

        databasedestroy = Dictionary of a database
        dimensiondestroy = Dictionary of dimension
        destroyelement = Dictionary of a Element to delete

        Return true if destroy element request OK
        Property response return '1' for OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelement(testdatabase, \
testdimension, 'element', '1'):
                                testelement = paloelement.response
                                print('Test element :', testelement)
                                if paloelement.destroyelement(testdatabase, \
testdimension, testelement):
                                    print('Deletes element')
                            else:
                                print(paloelement.errors)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'element/destroy'
        self.param = {'database': databasedestroy["id"],
                      'dimension': dimensiondestroy["id"],
                      'element': destroyelement["id"],
                      'sid': self.keydw
                      }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.errors += 'Palo Error destroy Element return : '
            self.errors += str(self.getdata())
            return False

    def destroyelementbulk(self, databasedestroybulk, dimensiondestroybulk,
                                                        listelementsdestroy):
        """
        Method destroyelementbulk()

        Deletes list of elements

        databasedestroy = Dictionary of a database
        dimensiondestroy = Dictionary of dimension
        destroyelement = Dictionary of a Element to delete

        Return true if destroy bulk element request OK
        Property response return '1' for OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelementbulk(testdatabase, \
testdimension, ['element1', 'element2', 'element3'], '1'):
                                print('element1, element2, element3 created')
                            else:
                                print(paloelement.errors)
                            if paloelement.getelementsdimension(testdatabase, \
testdimension):
                                print('Elements of database {0} and dimension \
{1} : '.format(testdatabase["name"], testdimension["name"]), \
paloelement.response)
                                elements = paloelement.response
                                if paloelement.destroyelementbulk(\
testdatabase, testdimension, elements)
                            else:
                                print(paloelement.errors)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        destroybulkelements = ''
        for elementdestroy in listelementsdestroy:
            destroybulkelements += elementdestroy["id"] + ','
        self.cmd = 'element/destroy_bulk'
        self.param = {'database': databasedestroybulk["id"],
                      'dimension': dimensiondestroybulk["id"],
                      'elements': destroybulkelements[:-1],
                      'sid': self.keydw
                      }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.errors += 'Palo Error destroy bulk Elements return : '
            self.errors += str(self.getdata())
            return False

    def infoelement(self, databaseinfo, dimensioninfo, infoelement):
        """
        Method infoelement()

        Shows the element data

        databaseinfo = Dictionary of a database
        dimensioninfo = Dictionary of dimension
        infoelement = Dictionary of a Element to info

        Return true if info element request OK
        Property response return dictionary element like :
                dictionary : {'id': Identifier of the element,
                              'name': Name of the element,
                              'position': Number of dimension in the database,
                              'level': Level of the element,
                              'indent': Indent of the element,
                              'depth': Depth of the element,
                              'type': Type of element (1=Numeric, 2=String, \
4=Consolidated),
                              'number_parents': Number of parents,
                              'parents': list of parent identifiers,
                              'number_children': Number of children,
                              'children': list of children identifiers,
                              'weights': list of children weight
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelement(testdatabase, \
testdimension, 'element', '1'):
                                testelement = paloelement.response
                            else:
                                print(paloelement.errors)
                            if paloelement.infoelement(testdatabase, \
testdimension, testelement):
                                print('Info element', paloelement.response)
                            else:
                                print(paloelement.errors)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'element/info'
        self.param = {'database': databaseinfo["id"],
                      'dimension': dimensioninfo["id"],
                      'element': infoelement["id"],
                      'sid': self.keydw
                      }
        if self.urlresult:
            response = self.getdata()
            infolistelement = {"id":None,
                               "name":None,
                               "position":None,
                               "level":None,
                               "indent":None,
                               "depth":None,
                               "type":None,
                               "number_parents":None,
                               "parents":None,
                               "number_children":None,
                               "children":None,
                               "weights":None,
                              }
            response = response.split(';')[:-1]
            infolistelement["id"] = response[0]
            infolistelement["name"] = response[1]
            infolistelement["position"] = response[2]
            infolistelement["level"] = response[3]
            infolistelement["indent"] = response[4]
            infolistelement["depth"] = response[5]
            infolistelement["type"] = response[6]
            infolistelement["number_parents"] = response[7]
            infolistelement["parents"] = response[8].split(',')
            infolistelement["number_children"] = response[9]
            infolistelement["children"] = response[10].split(',')
            infolistelement["weights"] = response[11].split(',')
            self.response = infolistelement.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error info Element return : '
            self.errors += str(self.getdata())
            return False

    def moveelement(self, databaseelmove, dimensionelmove, elementmove,
                                                                newposition):
        """
        Method moveelement()

        Changes the position of element

        databaseelmove = Dictionary of a database
        dimensionelmove = Dictionary of dimension
        elementmove = Dictionary of element
        newposition = New position of element

        Return true if move element request OK
        Property response return dictionary element like :
                dictionary : {'id': Identifier of the element,
                              'name': Name of the element,
                              'position': Number of dimension in the database,
                              'level': Level of the element,
                              'indent': Indent of the element,
                              'depth': Depth of the element,
                              'type': Type of element (1=Numeric, 2=String, \
4=Consolidated),
                              'number_parents': Number of parents,
                              'parents': list of parent identifiers,
                              'number_children': Number of children,
                              'children': list of children identifiers,
                              'weights': list of children weight
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelement(testdatabase, \
testdimension, 'el2', '1'):
                                testelement = paloelement.response
                                print('Test element :', testelement)
                            else:
                                print(paloelement.errors)
                            if paloelement.createelement(testdatabase, \
testdimension, 'el1', '1'):
                                test2element = paloelement.response
                                print('Test element :', test2element)
                            else:
                                print(paloelement.errors)
                            if paloelement.moveelement(testdatabase, \
testdimension, test2element, testelement["position"]):
                                print('Move el1 to el2 : ', \
paloelement.response)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'element/move'
        self.param = {'database': databaseelmove["id"],
                      'dimension': dimensionelmove["id"],
                      'element': elementmove["id"],
                      'position': newposition,
                      'sid': self.keydw
                      }
        if self.urlresult:
            responsemove = self.getdata()
            elementmove = {"id":None,
                                 "name":None,
                                 "position":None,
                                 "level":None,
                                 "indent":None,
                                 "depth":None,
                                 "type":None,
                                 "number_parents":None,
                                 "parents":None,
                                 "number_children":None,
                                 "children":None,
                                 "weights":None,
                                }
            responsemove = responsemove.split(';')[:-1]
            elementmove["id"] = responsemove[0]
            elementmove["name"] = responsemove[1]
            elementmove["position"] = responsemove[2]
            elementmove["level"] = responsemove[3]
            elementmove["indent"] = responsemove[4]
            elementmove["depth"] = responsemove[5]
            elementmove["type"] = responsemove[6]
            elementmove["number_parents"] = responsemove[7]
            elementmove["parents"] = responsemove[8].split(',')
            elementmove["number_children"] = responsemove[9]
            elementmove["children"] = responsemove[10].split(',')
            elementmove["weights"] = responsemove[11].split(',')
            self.response = elementmove.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error move Element return : '
            self.errors += str(self.getdata())
            return False

    def renameelement(self, databaserename, dimensionrename, elementrename,
                                                                newnameelement):
        """
        Method renameelement()

        Changes the name of an element

        databaserename = Dictionary of a database
        dimensionrename = Dictionary of dimension
        elementrename = Dictionary of element
        newnameelement = New name of the element

        Return true if move element request OK
        Property response return dictionary element like :
                dictionary : {'id': Identifier of the element,
                              'name': Name of the element,
                              'position': Number of dimension in the database,
                              'level': Level of the element,
                              'indent': Indent of the element,
                              'depth': Depth of the element,
                              'type': Type of element (1=Numeric, 2=String, \
4=Consolidated),
                              'number_parents': Number of parents,
                              'parents': list of parent identifiers,
                              'number_children': Number of children,
                              'children': list of children identifiers,
                              'weights': list of children weight
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelement(testdatabase, \
testdimension, 'elements', '4'):
                                testelement = paloelement.response
                                print('Test elements :', testelement)
                            else:
                                print(paloelement.errors)
                            if paloelement.renameelement(testdatabase, \
testdimension, testelement, 'consolided'):
                                testelement = paloelement.response
                                print('Rename element :', testelement)
                            else:
                                print(paloelement.errors)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'element/rename'
        self.param = {'database': databaserename["id"],
                      'dimension': dimensionrename["id"],
                      'element': elementrename["id"],
                      'new_name': newnameelement,
                      'sid': self.keydw
                      }

        if self.urlresult:
            responserename = self.getdata()
            elementrename = {"id":None,
                                 "name":None,
                                 "position":None,
                                 "level":None,
                                 "indent":None,
                                 "depth":None,
                                 "type":None,
                                 "number_parents":None,
                                 "parents":None,
                                 "number_children":None,
                                 "children":None,
                                 "weights":None,
                                }
            responserename = responserename.split(';')[:-1]
            elementrename["id"] = responserename[0]
            elementrename["name"] = responserename[1]
            elementrename["position"] = responserename[2]
            elementrename["level"] = responserename[3]
            elementrename["indent"] = responserename[4]
            elementrename["depth"] = responserename[5]
            elementrename["type"] = responserename[6]
            elementrename["number_parents"] = responserename[7]
            elementrename["parents"] = responserename[8].split(',')
            elementrename["number_children"] = responserename[9]
            elementrename["children"] = responserename[10].split(',')
            elementrename["weights"] = responserename[11].split(',')
            self.response = elementrename.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error rename Element return : '
            self.errors += str(self.getdata())
            return False

    def replaceelement(self, databasereplace, dimensionreplace, elementreplace,
        typeelreplace, listchildrenelreplace=None, listweightselreplace=None):
        """
        Method replaceelement()

        Creates or updates an element

        databasereplace = Dictionary of a database
        dimensionreplace = Dictionary of dimension
        elementreplace = Dictionary of element
        typeelreplace = Type of the element (1=Numeric, 2=String, \
4=Consolidated)
        listchildrenelreplace = List of children identifier (Only for type 4)
        listweightselreplace = List of children weight (Only for type 4, \
defaults weight=1)

        Return true if move element request OK
        Property response return dictionary element like :
                dictionary : {'id': Identifier of the element,
                              'name': Name of the element,
                              'position': Number of dimension in the database,
                              'level': Level of the element,
                              'indent': Indent of the element,
                              'depth': Depth of the element,
                              'type': Type of element (1=Numeric, 2=String, \
4=Consolidated),
                              'number_parents': Number of parents,
                              'parents': list of parent identifiers,
                              'number_children': Number of children,
                              'children': list of children identifiers,
                              'weights': list of children weight
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelement(testdatabase, \
testdimension, 'element', '1'):
                                testelement = paloelement.response
                                print('Test element :', testelement)
                            else:
                                print(paloelement.errors)
                            if paloelement.replaceelement(testdatabase, \
testdimension, testelement, '2'):
                                testelement = paloelement.response
                                print('Rplacement element type to 2 :', \
testelement)
                            else:
                                print(paloelement.errors)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'element/replace'
        if typeelreplace == '4':
            if listweightselreplace:
                self.param = {'database': databasereplace["id"],
                              'dimension': dimensionreplace["id"],
                              'element': elementreplace["id"],
                              'types': typeelreplace,
                              'children': \
        str(listchildrenelreplace).replace('\'', '').replace(' ', '')[1:-1],
                              'weights' : \
        str(listweightselreplace).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
            else:
                self.param = {'database': databasereplace["id"],
                              'dimension': dimensionreplace["id"],
                              'element': elementreplace["id"],
                              'types': typeelreplace,
                              'children': \
        str(listchildrenelreplace).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
        else:
            self.param = {'database': databasereplace["id"],
                          'dimension': dimensionreplace["id"],
                          'element': elementreplace["id"],
                          'type': typeelreplace,
                          'sid': self.keydw
                         }

        if self.urlresult:
            responsereplace = self.getdata()
            elementelreplace = {"id":None,
                                 "name":None,
                                 "position":None,
                                 "level":None,
                                 "indent":None,
                                 "depth":None,
                                 "type":None,
                                 "number_parents":None,
                                 "parents":None,
                                 "number_children":None,
                                 "children":None,
                                 "weights":None,
                                }
            responsereplace = responsereplace.split(';')[:-1]
            elementelreplace["id"] = responsereplace[0]
            elementelreplace["name"] = responsereplace[1]
            elementelreplace["position"] = responsereplace[2]
            elementelreplace["level"] = responsereplace[3]
            elementelreplace["indent"] = responsereplace[4]
            elementelreplace["depth"] = responsereplace[5]
            elementelreplace["type"] = responsereplace[6]
            elementelreplace["number_parents"] = responsereplace[7]
            elementelreplace["parents"] = responsereplace[8].split(',')
            elementelreplace["number_children"] = responsereplace[9]
            elementelreplace["children"] = responsereplace[10].split(',')
            elementelreplace["weights"] = responsereplace[11].split(',')
            self.response = elementelreplace.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error replace Element return : '
            self.errors += str(self.getdata())
            return False

    def replaceelementbulk(self, databasereplacebulk, dimensionreplacebulk,
        elementsreplacebulk, typeelreplacebulk, listchildrenelreplacebulk=None,
                                                listweightselreplacebulk=None):
        """
        Method replaceelementbulk()

        Creates or updates an element

        databasereplacebulk = Dictionary of a database
        dimensionreplacebulk = Dictionary of a dimension
        elementsreplacebulk = List of dictionary of elements
        typeelreplacebulk = Type of the element (-=Numeric, 2=String, \
4=Consolidated)
        listchildrenelreplacebulk = List of children identifier (Only for type \
4)
        listweightselreplacebulk = List of children weight (Only for type 4, \
defaults weight=1)

        Return true if move element request OK
        Property response return '1' for OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloelement import PypaloElement

        paloelement = PypaloElement('127.0.0.1', '7777')
        if paloelement.login('admin', 'admin'):
            print('Connection Serveur OK')
            if paloelement.getdatabases():
                databases = paloelement.response
                if paloelement.getdimensionsdatabase(databases[0]):
                    dimensions = paloelement.response
                    if paloelement.createdatabase("test"):
                        testdatabase = paloelement.response
                        if paloelement.infodatabase(testdatabase):
                            print('Info test database : ', \
paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.createdimension(testdatabase, \
'testdim'):
                            testdimension = paloelement.response
                            print('Creating dimension testdim : ', \
paloelement.response)
                            if paloelement.createelement(testdatabase, \
testdimension, 'el0', '1'):
                                testelement = paloelement.response
                                print('Test elements :', testelement)
                            else:
                                print(paloelement.errors)
                            if paloelement.createelement(testdatabase, \
testdimension, 'el1', '1'):
                                test2element = paloelement.response
                                print('Test element :', test2element)
                            else:
                                print(paloelement.errors)
                            if paloelement.replaceelementbulk(testdatabase, \
testdimension, [testelement, test2element], '2'):
                                if paloelement.getelementsdimension(\
testdatabase, testdimension):
                                    print('Convert el1 and el2 to type 2', \
paloelement.response)
                            if paloelement.destroydimension(testdatabase, \
testdimension):
                            else:
                                print(paloelement.errors)
                        else:
                            print(paloelement.errors)
                        if paloelement.destroydatabase(testdatabase)
                            print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
            if paloelement.logout():
                print('logout OK')
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        ~~~~~~~~~~~~~
        """
        listelreplacebulk = ''
        for elementreplace in elementsreplacebulk:
            listelreplacebulk += elementreplace["id"] + ','
        self.cmd = 'element/replace_bulk'
        if typeelreplacebulk == '4':
            if listweightselreplacebulk:
                self.param = {'database': databasereplacebulk["id"],
                              'dimension': dimensionreplacebulk["id"],
                              'elements': listelreplacebulk[:-1],
                              'types': typeelreplacebulk,
                              'children': \
    str(listchildrenelreplacebulk).replace('\'', '').replace(' ', '')[1:-1],
                              'weights' : \
    str(listweightselreplacebulk).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
            else:
                self.param = {'database': databasereplacebulk["id"],
                              'dimension': dimensionreplacebulk["id"],
                              'elements': listelreplacebulk[:-1],
                              'types': typeelreplacebulk,
                              'children': \
    str(listchildrenelreplacebulk).replace('\'', '').replace(' ', '')[1:-1],
                              'sid': self.keydw
                             }
        else:
            self.param = {'database': databasereplacebulk["id"],
                          'dimension': dimensionreplacebulk["id"],
                          'elements': listelreplacebulk[:-1],
                          'type': typeelreplacebulk,
                          'sid': self.keydw
                         }

        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error replace bulk Element return : '
            self.errors += str(self.getdata())
            return False


################################################################################

if __name__ == "__main__":
    paloelement = PypaloElement('127.0.0.1', '7777')
    if paloelement.login('admin', 'admin'):
        print('Connection Serveur OK')
        if paloelement.getdatabases():
            palodatabases = paloelement.response
            if paloelement.getdimensionsdatabase(palodatabases[0]):
                paloelements = paloelement.response
                if paloelement.createdatabase("test"):
                    print('Database test created')
                    testdatabase = paloelement.response
                    if paloelement.infodatabase(testdatabase):
                        print('Info test database : ', paloelement.response)
                    else:
                        print(paloelement.errors)
                    if paloelement.createdimension(testdatabase, 'testdim'):
                        testdimension = paloelement.response
                        print('Creating dimension testdim : ', \
                                                        paloelement.response)
                        if paloelement.createelement(testdatabase, \
                                            testdimension, 'testelement1', '1'):
                            print('Creating element testelement1 : ', \
                                                        paloelement.response)
                            testelement1 = paloelement.response
                        else:
                            print(paloelement.errors)
                        if paloelement.replaceelement(testdatabase, \
                                testdimension, testelement1, '2'):
                            print('Replace testelement1 by type 2 : ', \
                                                        paloelement.response)
                            testelement1 = paloelement.response
                        else:
                            print(paloelement.errors)
                        if paloelement.createelementbulk(testdatabase, \
                        testdimension, ['testelement2', 'testelement3'], '1'):
                            print('testelement2 and testelement3 created')
                        else:
                            print(paloelement.errors)
                        if paloelement.infoelement(testdatabase, testdimension,
                                                                   {'id':'1'}):
                            print('Info element2 : ', paloelement.response)
                            testelement2 = paloelement.response
                        else:
                            print(paloelement.errors)
                        if paloelement.infoelement(testdatabase, testdimension,
                                                                   {'id':'2'}):
                            print('Info element3 : ', paloelement.response)
                            testelement3 = paloelement.response
                        else:
                            print(paloelement.errors)
                        if paloelement.replaceelementbulk(testdatabase, \
                        testdimension, [testelement2, testelement3], '2'):
                            print('Replace testelement2 and testelement3 \
by type 2 OK')
                        else:
                            print(paloelement.errors)
                        if paloelement.createelement(testdatabase, \
                            testdimension, 'testelement4', '4', ['0', '1']):
                            print('testelement4 created : ', \
                                                        paloelement.response)
                            testelement4 = paloelement.response
                        else:
                            print(paloelement.errors)
                        if paloelement.appendelement(testdatabase, \
                            testdimension, testelement4, ['2']):
                            print('Append child testelement3 to testelement4')
                        else:
                            print(paloelement.errors)
                        if paloelement.getelementsdimension(testdatabase,
                                                                testdimension):
                            print('Elements of database {0} and dimension \
{1}'.format(testdatabase["name"], testdimension["name"]))
                            print(paloelement.response)
                        else:
                            print(paloelement.errors)
                        if paloelement.moveelement(testdatabase, testdimension,
                                        testelement4, testelement1["position"]):
                            print('Move element4 to first position OK :', \
                                                        paloelement.response)
                            testelement4 = paloelement.response
                        else:
                            print(paloelement.errors)
                        if paloelement.renameelement(testdatabase,
                            testdimension, testelement4, 'ConsolidatedEl'):
                            print('Rename element4 to ConsolidatedEl OK :', \
                                                        paloelement.response)
                            testelement4 = paloelement.response
                        else:
                            print(paloelement.errors)
                        if paloelement.destroyelement(testdatabase, \
                    testdimension, testelement4):
                            print('Deletes testelement4 : ', \
                                                        paloelement.response)
                        else:
                            print(paloelement.errors)
                    if paloelement.getelementsdimension(testdatabase,
                                                                testdimension):
                        elementstestdimension = paloelement.response
                        if paloelement.destroyelementbulk(testdatabase,
                                        testdimension, elementstestdimension):
                            print('Destroy bulk testelement1-3 OK')
                        else:
                            print(paloelement.errors)
                    else:
                        print(paloelement.errors)
                    if paloelement.infodimension(testdatabase, testdimension):
                        print('Info dimension testdim : ', \
                                                        paloelement.response)
                    else:
                        print(paloelement.errors)
                    if paloelement.destroydimension(testdatabase, \
                                                                testdimension):
                        print('Dimension testdim deleted : ', \
                                                        paloelement.response)
                    else:
                        print(paloelement.errors)
                    if paloelement.destroydatabase(testdatabase):
                        print('Database test deleted : ', \
                                                        paloelement.response)
                    else:
                        print(paloelement.errors)
                else:
                    print(paloelement.errors)
            else:
                print(paloelement.errors)
        else:
            print(paloelement.errors)
        if paloelement.logout():
            print('logout OK')
        else:
            print(paloelement.errors)
    else:
        print(paloelement.errors)
