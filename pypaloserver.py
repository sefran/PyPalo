"""
File pypaloserver.py
Server Request Class for Palo Jedox MOLAP Database
Contains Server Class Palo API

@author Franc SERRES aka sefran
@version 0.5
@licence GPL V3
"""

from pypalo.pypalorequest import PypaloRequestURL

class PypaloServer(PypaloRequestURL):
    """
    Private Constructor Method PypaloServer('Host Palo', 'Port Host Palo')
    PypaloServer = Object pointer of Configuration

    usage:
    ~~~~~~~~~~~~~{.py}
    from pypalo.pypaloserver import PypaloServer

    paloserver = PypaloServer('Host', 'Port')
    ~~~~~~~~~~~~~
    """
    def __init__(self, host, port):
        """
        host = Ip adress of host like '127.0.0.1'
        port = Number of port like '7777'
        """
        PypaloRequestURL.__init__(self)
        PypaloRequestURL.host = host
        PypaloRequestURL.port = port
        self.__response = None

    def _getresponse(self):
        """
        Property response

        Return result of request palo server

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        paloserver.response = True
        print('Response of server request : ', paloserver.response)
        ~~~~~~~~~~~~~
        """
        return self.__response

    def _setresponse(self, val=True):
        """ internal set of response """
        self.__response = val if val else self.__response

    response = property(_getresponse, _setresponse)

    def getdatabases(self):
        """
        Method getdatabases()

        Returns the list of databases.

        Return true if returning the list of database
        Property response return list of dictionary database :
            list[
                dictionary{'id': Identifier of the database,
                         'name': Name of the database,
            'number_dimensions': Number of dimensions in the
                                 database,
                 'number_cubes': Number of cubes in the database,
                       'status': Status of database (0 is unloaded, 1 is loaded
                                 and 2 is changed),
                         'type': Type of database (0 is normal, 1 is System
                                 and 3 is user info),
                        'token': The database token of database
                }
            ]

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloserver import PypaloServer

        paloserver = PypaloServer('127.0.0.1', '7777')
        print('Host :', paloserver.host)
        print('Port :', paloserver.port)
        if paloserver.login('admin', 'admin'):
            paloserver.getdatabases()
            print('Database list :', paloserver.response)
            paloserver.logout()
        else:
            print(paloserver.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'server/databases'
        self.param = {'sid' : self.keydw,
                      'show_normal' : int(self.shownormal),
                      'show_system' : int(self.showsystem),
                      'show_user_info' : int(self.showuserinfo),
                      }
        if self.urlresult:
            if self.getdata() == '\'':
                self.response = None
                return True
            database = {"id":None,
                         "name":None,
                         "number_dimensions":None,
                         "number_cubes":None,
                         "status":None,
                         "type":None,
                         "token":None
                         }
            listdatabase = []
            response = self.getdata()
            response = response.split('\n')[:-1]
            for elementlist in response:
                elementlist = elementlist.split(';')[:-1]
                database["id"] = elementlist[0]
                database["name"] = elementlist[1]
                database["number_dimensions"] = elementlist[2]
                database["number_cubes"] = elementlist[3]
                database["status"] = elementlist[4]
                database["type"] = elementlist[5]
                database["token"] = elementlist[6]
                listdatabase.append(database.copy())
            self.response = listdatabase.copy()
            self.errors = None
            return True
        else:
            self.errors = 'Palo Error server returns the list of databases : '
            self.errors += str(self.getdata())
            return False

    def getinfoserver(self):
        """
        Method getinfoserver()

        Information about the server

        Return true if get info from server
        Property response return :
        dictonary {'major_version': major version of the server,
                'minor_verversion': minor version of the server,
                  'bugfix_version': bugfix level of the server,
                    'build_number': build number of the server,
                      'encryption': 0 for none-1 for optional-2 for required,
                      'https_port': the corresponding HTTPS port or 0 if HTTPS
                                    is not supported
                }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloserver import PypaloServer

        paloserver = PypaloServer('127.0.0.1', '7777')
        print('Host :', paloserver.host)
        print('Port :', paloserver.port)
        if paloserver.getinfoserver():
            print('Info server : ', paloserver.response)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'server/info'
        self.param = {}
        if self.urlresult:
            database = {"major_version":None,
                        "minor_verversion":None,
                        "bugfix_version":None,
                        "build_number":None,
                        "encryption":None,
                        "https_port":None
                        }
            response = self.getdata()
            response = response.split(";")[:-1]
            database["major_version"] = response[0]
            database["minor_verversion"] = response[1]
            database["bugfix_version"] = response[2]
            database["build_number"] = response[3]
            database["encryption"] = response[4]
            database["https_port"] = response[5]
            self.response = database
            self.errors = None
            return True
        else:
            self.errors = 'Palo Error server return about info : '
            self.errors += str(self.getdata())
            return False

    def getlicense(self):
        """
        Method getlicense()

        Information about the server license

        Return true if get license
        Property response return :
        dictonary {'username': User Name of user server license,
                   'numusers': Number of users,
             'expirationdate': date of expiration license,
                       'type': License type,
                }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloserver import PypaloServer

        paloserver = PypaloServer('127.0.0.1', '7777')
        print('Host :', paloserver.host)
        print('Port :', paloserver.port)
        if paloserver.getlicense():
            print('License Palo : ', paloserver.response)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'server/license'
        self.param = {}
        if self.urlresult:
            palolicense = {"username":None,
                       "numusers":None,
                       "expirationdate":None,
                       "type":None
                       }
            response = self.getdata()
            response = response.split(";")[:-1]
            palolicense["username"] = response[0]
            palolicense["numusers"] = response[1]
            palolicense["expirationdate"] = response[2]
            palolicense["type"] = response[3]
            self.response = license
            self.errors = None
            return True
        else:
            self.errors = 'Palo Error server return about the server license : '
            self.errors += str(self.getdata())
            return False

    def loadserver(self):
        """
        Method loadserver()

        Reloads the server from disk

        Return true if reloading the server from disk
        Property response return 1 if reloading

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloserver import PypaloServer

        paloserver = PypaloServer('127.0.0.1', '7777')
        print('Host :', paloserver.host)
        print('Port :', paloserver.port)
        if paloserver.login('admin', 'admin'):
            print('Login keep time Connection : \
{0}s'.format(paloserver.response))
            print('Keyword Data Warehouse :', paloserver.keydw)
            if paloserver.saveserver():
                print('Save the server from disk OK', paloserver.response)
            else:
                print(paloserver.errors)
            if paloserver.shutdownserver():
                print('Shutdown the server from disk OK', paloserver.response)
            else:
                print(paloserver.errors)
            if paloserver.loadserver():
                print('Reloads the server from disk OK', paloserver.response)
            else:
                print(paloserver.errors)
            paloserver.logout()
            print('logout :', paloserver.response)
        else:
            print(paloserver.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'server/load'
        self.param = {'sid' : self.keydw}
        if self.urlresult:
            response = self.getdata()
            self.response = response[0]
            self.errors = None
            return True
        else:
            response = self.getdata()
            if response[0] == '1':
                self.response = response[0]
                return True
            self.errors = 'Palo Error reloading the server from disk : '
            self.errors += response
            return False

    def getsid(self):
        """
        Method getsid()

        Get the session identifier for a server connection

        Return true if get session identifier
        Property keydw return session identifier for server connection
        Property response return timeout intervall in seconds
            If no request is made within this intervall the session becomes
            inactive

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloserver import PypaloServer

        paloserver = PypaloServer('Host', 'Port')
        if paloserver.getsid():
            print('Keyword Data Warehouse :', paloserver.keydw)
            print('Expiration time session : ', paloserver.response)
            paloserver.logout()
        else:
            print(paloserver.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'server/login'
        self.param = {'user' : self.user, 'password' : self.getmd5password()}
        if self.urlresult:
            response = self.getdata()
            response = response.split(";")[:-1]
            self.keydw = response[0]
            self.response = response[1]
            self.errors = None
            return True
        else:
            self.errors = 'Palo Error server request login and get SID : '
            self.errors += str(self.getdata())
            return False

    def login(self, user=None, password=None):
        """
        Method login(user, password)

        This request is used for user authentication and to get session \
identifier (sid)

        Return true if login to server
        Property keydw return session identifier for server connection
        Property response return timeout intervall in seconds
            If no requests are made within this intervall the session becomes
            inactive

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloserver import PypaloServer

        paloserver = PypaloServer('Host', 'Port')
        if paloserver.login('admin', 'admin'):
            Print('Connection Serveur OK')
            paloserver.logout()
        else:
            print(paloserver.errors)
        ~~~~~~~~~~~~~
        """
        if user != None:
            self.user = user
        else:
            self.errors = 'Palo Error request login : None user'
            return False
        if password != None:
            self.password = password
        else:
            self.errors = 'Palo Error request login : None password'
            return False
        if self.getsid():
            return True
        else:
            return False

    def logout(self):
        """
        Method logout()

        This request is used to close a connection

        Return true if request correct logout
        Property response return 1 if request correct logout

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloserver import PypaloServer

        paloserver = PypaloServer('Host', 'Port')
        if paloserver.login('admin', 'admin'):
            Print('Connection Serveur OK')
            paloserver.logout()
        else:
            print(paloserver.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'server/logout'
        self.param = {'sid' : self.keydw}
        if self.urlresult:
            response = self.getdata()
            self.response = response[0]
            self.errors = None
            return True
        else:
            self.errors = 'Palo Error server request logout : '
            self.errors += str(self.getdata())
            return False

    def saveserver(self):
        """
        Method saveserver()

        Saves the server to disk

        Return true if save the server to disk is OK
        Property response return 1 if save is OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloserver import PypaloServer

        paloserver = PypaloServer('127.0.0.1', '7777')
        print('Host :', paloserver.host)
        print('Port :', paloserver.port)
        if paloserver.login('admin', 'admin'):
            print('Login keep time Connection : \
{0}s'.format(paloserver.response))
            print('Keyword Data Warehouse :', paloserver.keydw)
            if paloserver.saveserver():
                print('Save the server from disk OK', paloserver.response)
            else:
                print(paloserver.errors)
            paloserver.logout()
            print('logout :', paloserver.response)
        else:
            print(paloserver.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'server/save'
        self.param = {'sid' : self.keydw}
        if self.urlresult:
            response = self.getdata()
            self.response = response[0]
            self.errors = None
            return True
        else:
            self.errors = 'Palo Error save the server to disk : '
            self.errors += str(self.getdata())
            return False

    def shutdownserver(self):
        """
        Method shutdownserver()

        Shuts down the server

        Return true if shuting down the server
        Property response return 1 if shuting is OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloserver import PypaloServer

        paloserver = PypaloServer('127.0.0.1', '7777')
        print('Host :', paloserver.host)
        print('Port :', paloserver.port)
        if paloserver.login('admin', 'admin'):
            print('Login keep time Connection : \
{0}s'.format(paloserver.response))
            print('Keyword Data Warehouse :', paloserver.keydw)
            if paloserver.saveserver():
                print('Save the server from disk OK', paloserver.response)
            else:
                print(paloserver.errors)
            if paloserver.shutdownserver():
                print('Shutdown the server from disk OK', paloserver.response)
            else:
                print(paloserver.errors)
            if paloserver.loadserver():
                print('Reloads the server from disk OK', paloserver.response)
            else:
                print(paloserver.errors)
            paloserver.logout()
            print('logout :', paloserver.response)
        else:
            print(paloserver.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'server/shutdown'
        self.param = {'sid' : self.keydw}
        if self.urlresult:
            response = self.getdata()
            self.response = response[0]
            self.errors = None
            return True
        else:
            self.errors = 'Palo Error shuts the server : ' + self.getdata()
            return False

################################################################################

if __name__ == "__main__":
    paloserver = PypaloServer('127.0.0.1', '7777')
    print('Host :', paloserver.host)
    print('Port :', paloserver.port)
    if paloserver.getinfoserver():
        print('Info server :', paloserver.response)
    if paloserver.getlicense():
        print('License Palo :', paloserver.response)
    if paloserver.login('admin', 'admin'):
        print('Login keep time Connection : {0}s'.format(paloserver.response))
        print('Keyword Data Warehouse :', paloserver.keydw)
        if paloserver.saveserver():
            print('Save the server from disk OK', paloserver.response)
        else:
            print(paloserver.errors)
        if paloserver.shutdownserver():
            print('Shutdown the server from disk OK', paloserver.response)
        else:
            print(paloserver.errors)
        if paloserver.loadserver():
            print('Reloads the server from disk OK', paloserver.response)
        else:
            print(paloserver.errors)
        paloserver.getdatabases()
        print('Param :', paloserver.param)
        print('Database list :', paloserver.response)
        paloserver.logout()
        print('logout :', paloserver.response)
    else:
        print(paloserver.errors)
