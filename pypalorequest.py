"""
File pypalorequest.py
Request Class for Palo Jedox MOLAP Database
Contains Request Server fonctions

@author Franc SERRES aka sefran Original author Fabio MARRAS with changes from
Andrejs PRIKULIS aka HIGH-Zen
@version 0.5
@licence GPL V3
"""
__version_info__ = ('0', '8', '0')
__version__ = '.'.join(__version_info__)
__author__ = 'Franc SERRES'
__licence__ = 'GPLv3'
__copyright__ = '(c) 2014 by Franc SERRES aka sefran'
__all__ = ['PypaloRequestURL']

import socket
import urllib3
import hashlib
import re

ip = re.compile("^([0-9]{1,3}\.){3}[0-9]{1,3}?$")

class PypaloRequestURL(object):
    """
    Private Constructor Method PypaloRequestURL()
    PypaloRequestURL = Object pointer of Configuration

    usage:
    ~~~~~~~~~~~~~{.py}
    from pypalo.pypalorequest import PypaloRequestURL

    palorequesturl = PypaloRequestURL()

    palorequesturl.host = '127.0.0.1'
    palorequesturl.port = '7777'
    palorequesturl.cmd = 'server/login'
    palorequesturl.user = 'admin'
    palorequesturl.password = 'admin'
    palorequesturl.param = {'user' : palorequesturl.user,
                         'password' : palorequesturl.getmd5password()}
    if palorequesturl.urlresult:
        print(palorequesturl.getdata())
    else:
        print('Connection KO')
    ~~~~~~~~~~~~~
    """
    def __init__(self):
        """  """
        self.__host = None
        self.__hostname = None
        self.__port = None
        self.__user = None
        self.__password = None
        self.__keydw = None
        self.__proxyurl = None
        self.__useproxy = False
        self.__cmd = None
        self.__param = None
        self.__requestheader = 'GET'
        self.__data = None
        self.__errors = None
        self.__serverroot = None
        self.__url = None
        self.__urlresult = None

    def _gethost(self):
        """
        Property host

        host = Palo Server IP adress name like '127.0.0.1'

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.host = '127.0.0.1'
        print('Adress host : ', paloserver.host)
        ~~~~~~~~~~~~~
        """
        return self.__host

    def _sethost(self, val=True):
        """ internal set of host """
        if ip.match(val):
            self.__host = val
            try:
                self.__hostname = socket.gethostbyaddr(val)
            except:
                raise NameError('can\'t contact {0}'.format(val))
        else:
            self.__hostname = val
            try:
                self.__host = socket.gethostbyname(val)
            except:
                raise NameError('{0} wrong hostname'.format(val))

    host = property(_gethost, _sethost)

    def _gethostname(self):
        """
        Property hostname

        hostname = Palo Server address name like 'localhost'

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.hostname = 'localhost'
        print('Host name : ', paloserver.hostname)
        ~~~~~~~~~~~~~
        """
        return self.__hostname

    hostname = property(_gethostname, _sethost)

    def _getport(self):
        """
        Property port


        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.port = '7777'
        print('Port host : ', paloserver.port)
        ~~~~~~~~~~~~~
        """
        return self.__port

    def _setport(self, val=True):
        """ internal set of port """
        self.__port = val if val else self.__port

    port = property(_getport, _setport)

    def _getuser(self):
        """
        Property user

        user = User identification of Palo Server like 'admin'

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.user = 'admin'
        print('User name : ', paloserver.user)
        ~~~~~~~~~~~~~
        """
        return self.__user

    def _setuser(self, val=True):
        """ internal set of user """
        self.__user = val if val else self.__user

    user = property(_getuser, _setuser)

    def _getpassword(self):
        """
        Property password

        password = Password of user identification like 'admin'

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.password = 'admin'
        print('User password : ', paloserver.password)
        ~~~~~~~~~~~~~
        """
        return self.__password

    def _setpassword(self, val=True):
        """ internal set of password """
        self.__password = val if val else self.__password

    password = property(_getpassword, _setpassword)

    def _getkeydw(self):
        """
        Property keydw

        keydw = Set Data Warehouse Keyword of http connection like 'AbcD'

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.keydw = 'ApGn'
        print('Session identifier : ', paloserver.keydw)
        ~~~~~~~~~~~~~
        """
        return self.__keydw

    def _setkeydw(self, val=True):
        """ internal set of  """
        self.__keydw = val if val else self.__keydw

    keydw = property(_getkeydw, _setkeydw)

    def _getproxyurl(self):
        """
        Property proxyurl

        proxyurl = Chose the URL of proxy like 'http://proxy:port/'

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.proxyurl = 'http://proxy:port/'
        print('Proxy address : ', paloserver.proxyurl)
        ~~~~~~~~~~~~~
        """
        return self.__useproxy

    def _setproxyurl(self, val=True):
        """ internal set of  """
        self.__proxyurl = val if val else self.__proxyurl

    proxyurl = property(_getproxyurl, _setproxyurl)

    def _getuseproxy(self):
        """
        Property useproxy

        useproxy = Chose use of proxy

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.useproxy = True
        print('Use of proxy : ', paloserver.useproxy)
        paloserver.useproxy = False
        print('Use of proxy : ', paloserver.useproxy)
        ~~~~~~~~~~~~~
        """
        return self.__useproxy

    def _setuseproxy(self, val=True):
        """ internal set of useproxy """
        self.__useproxy = True if val else False

    useproxy = property(_getuseproxy, _setuseproxy)

    def _getcmd(self):
        """
        Property cmd

        cmd = Palo line command like 'server/login'

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.cmd = 'server/login'
        print('Palo command : ', paloserver.cmd)
        ~~~~~~~~~~~~~
        """
        return self.__cmd

    def _setcmd(self, val=True):
        """ internal set of cmd """
        self.__cmd = val if val else self.__cmd

    cmd = property(_getcmd, _setcmd)

    def _getparam(self):
        """
        Property param

        param = Parameters of Palo command execution like {'user' : \
palorequesturl.user, 'password' : palorequesturl.getmd5password()}


        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.param = {'user' : \
palorequesturl.user, 'password' : palorequesturl.getmd5password()}
        print('Palo command parameter : ', paloserver.param)
        ~~~~~~~~~~~~~
        """
        return self.__param

    def _setparam(self, val=True):
        """ internal set of param """
        self.__param = val if val else self.__param

    param = property(_getparam, _setparam)

    def _getrequestheader(self):
        """
        Property requestheader

        requestheader = Parameter like 'GET'

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.requestheader = 'GET'
        print('Palo request header : ', paloserver.requestheader)
        ~~~~~~~~~~~~~
        """
        return self.__requestheader

    def _setrequestheader(self, val=True):
        """ internal set of requestheader """
        self.__requestheader = val if val else self.__requestheader

    requestheader = property(_getrequestheader, _setrequestheader)

    def _geterrors(self):
        """
        Property errors

        errors = Messages errors from request

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.errors = 'Some errors'
        print('Return errors : ', paloserver.errors)
        ~~~~~~~~~~~~~
        """
        return self.__errors

    def _seterrors(self, val=True):
        """ internal set of errors """
        self.__errors = val if val else self.__errors

    errors = property(_geterrors, _seterrors)

    def _getserverroot(self):
        """
        Property serverroot

        return http path root palo server like 'http://127.0.0.1:7777/'

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        paloserver.host = '127.0.0.1'
        paloserver.port = '7777'
        print('Path root palo : ', paloserver.serverroot)
        ~~~~~~~~~~~~~
        """
        self.__serverroot = "http://%s:%s/" % (self.host, self.port)
        return self.__serverroot

    serverroot = property(_getserverroot, None)

    def _geturl(self):
        """
        Property url

        Return request command palo url like http://127.0.0.1:7777/server/login

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        palorequesturl = PypaloRequestURL()

        print('Server Host : ', palorequesturl.host)
        print('Server Port : ', palorequesturl.port)
        print('Server Root : ', palorequesturl.cmd)
        print('Server Root : ', palorequesturl.url)
        ~~~~~~~~~~~~~
        """
        self.__url = self.serverroot + self.cmd
        return self.__url

    url = property(_geturl, None)

    def _geturlresult(self):
        """
        Property urlresult

        Return test of connection to server (True or False)

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        palorequesturl = PypaloRequestURL()

        palorequesturl.host = 'MyHost'
        print('Server Host : ', palorequesturl.host)
        palorequesturl.port = 'MyPort'
        print('Server Port : ', palorequesturl.port)
        palorequesturl.cmd = 'MyCMD'
        print('Server Root : ', palorequesturl.cmd)
        print('Server Root : ', palorequesturl.url)
        palorequesturl.param = 'MyParam'
        print('Server Param : ', palorequesturl.param)

        if palorequesturl.urlresult:
            print('Request URL OK')
        else:
            print('Bad Request URL')
        ~~~~~~~~~~~~~
        """
        #TODO Test connection host
        requesturl = urllib3.connection_from_url(self.serverroot).request(\
                                    self.requestheader, self.url, self.param)
        if requesturl.status != 200:
            resultdata = requesturl.data.decode('utf8').split(';')
            errstr = 'Connection KO, Error : '
            self.errors = errstr + str(resultdata)
            return False
        else:
            self.__data = requesturl.data.decode('utf8')
            return True

    urlresult = property(_geturlresult, None)

    def getdata(self):
        """
        Method getdata()

        Result of palo request command

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        palorequesturl = PypaloRequestURL()

        palorequesturl.host = 'MyHost'
        print('Server Host : ', palorequesturl.host)
        palorequesturl.port = 'MyPort'
        print('Server Port : ', palorequesturl.port)
        palorequesturl.cmd = 'MyCMD'
        print('Server Root : ', palorequesturl.cmd)
        print('Server Root : ', palorequesturl.url)
        palorequesturl.param = 'MyParam'
        print('Server Param : ', palorequesturl.param)

        if palorequesturl.urlresult:
            print(str(palorequesturl.getdata()))
        else:
            print('Bad Request URL')
        ~~~~~~~~~~~~~
        """
        return self.__data

    def getmd5password(self):
        """
        Method getmd5password()

        Return the password property MD5 encoded

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalorequest import PypaloRequestURL

        paloserver = PypaloRequestURL()

        print('MD5 Password : ', paloserver.getmd5password())
        paloserver.password = 'new password'
        print('New MD5 Password : ', paloserver.getmd5password())
        ~~~~~~~~~~~~~
        """
        return hashlib.md5(self.password.encode()).hexdigest()

if __name__ == "__main__":
    palorequesturl = PypaloRequestURL()
    palorequesturl.host = 'localhost'
    palorequesturl.port = '7777'
    palorequesturl.user = 'admin'
    palorequesturl.password = 'admin'
    print('Server Host : ', palorequesturl.host)
    print('Server Hostname : ', palorequesturl.hostname)
    print('Server Port : ', palorequesturl.port)
    palorequesturl.cmd = 'server/login'
    print('Server CMD : ', palorequesturl.cmd)
    print('Server url :', palorequesturl.url)
    print('User : ', palorequesturl.user)
    print('Password : ', palorequesturl.password)
    palorequesturl.param = {'user' : palorequesturl.user,
                         'password' : palorequesturl.getmd5password()}
    print('Server Param : ', palorequesturl.param)
    if palorequesturl.urlresult:
        print('Result request server/login : ', palorequesturl.getdata())
        keydatawarehouse = palorequesturl.getdata()[:-1].split(';')[0]
        palorequesturl.cmd = 'server/logout'
        print('Server CMD : ', palorequesturl.cmd)
        palorequesturl.param = {'sid' : keydatawarehouse}
        print('Server Param : ', palorequesturl.param)
        if palorequesturl.urlresult:
            print('Result request logout', palorequesturl.getdata())
    else:
        print('Connection KO', palorequesturl.errors)
