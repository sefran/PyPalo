# -*- coding: utf-8 -*-
"""
    File pypaloconfig.py
    --------------------
    Configuration Class for Palo Jedox MOLAP Database
    Contains Configuration Info

    :copyright: (c) 2014 by Franc SERRES aka sefran.
    :version: 0.8
    :licence: GPL V3 see LICENCE for more details.
"""

__version_info__ = ('0', '8', '0')
__version__ = '.'.join(__version_info__)
__author__ = 'Franc SERRES'
__licence__ = 'GPLv3'
__copyright__ = '(c) 2014 by Franc SERRES aka sefran'
__all__ = ['PypaloConfig']



class PypaloConfig(object):
    """
    Private Constructor Method PypaloConfig
    PypaloConfig = Object pointer of Configuration

    usage:
    ~~~~~~~~~~~~~{.py}
    from pypalo.pypaloconfig import PypaloConfig

    paloserver = PypaloConfig()
    ~~~~~~~~~~~~~
    """
    def __init__(self):
        """  """
        self.__shownormal = True
        self.__showsystem = False
        self.__showuserinfo = False
        self.__showattribute = False
        self.__showinfo = False
        self.__showgputype = True
        self.__useidentifier = False
        self.__showrule = False
        self.__showlockinfo = False
        self.__eventprocessor = True

    def _getshownormal(self):
        """
        Property shownormal

        shownormal = Show database of type Normal from palo server

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode normal : ', paloserver.shownormal)
        paloserver.shownormal = False
        print('Mode normal set false : ', paloserver.shownormal)
        ~~~~~~~~~~~~~
        """
        return self.__shownormal

    def _setshownormal(self, val=True):
        """ internal set of shownormal """
        self.__shownormal = True if val else False

    shownormal = property(_getshownormal, _setshownormal)

    def _getshowsystem(self):
        """
        Property showsystem

        showsystem = Show database of type System from palo server

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode system : ', paloserver.showsystem)
        paloserver.showsystem = True
        print('Mode system set true : ', paloserver.showsystem)
        ~~~~~~~~~~~~~
        """
        return self.__showsystem

    def _setshowsystem(self, val=True):
        """ internal set of showsystem """
        self.__showsystem = True if val else False

    showsystem = property(_getshowsystem, _setshowsystem)

    def _getshowuserinfo(self):
        """
        Property showuserinfo

        showuserinfo = Show database of type user info from palo server

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode show user info : ', paloserver.showuserinfo)
        paloserver.showuserinfo = True
        print('Mode show user info set true : ', paloserver.showuserinfo)
        ~~~~~~~~~~~~~
        """
        return self.__showuserinfo

    def _setshowuserinfo(self, val=True):
        """ internal set of showuserinfo """
        self.__showuserinfo = True if val else False

    showuserinfo = property(_getshowuserinfo, _setshowuserinfo)

    def _getshowattribute(self):
        """
        Property showattribute

        showattribute = Show cubes of type attributes

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode show attribute : ', paloserver.showattribute)
        paloserver.showattribute = True
        print('Mode show attribute set true : ', paloserver.showattribute)
        ~~~~~~~~~~~~~
        """
        return self.__showattribute

    def _setshowattribute(self, val=True):
        """ internal set of showattribute """
        self.__showattribute = True if val else False

    showattribute = property(_getshowattribute, _setshowattribute)

    def _getshowinfo(self):
        """
        Property showinfo

        showinfo = Show cubes of type user info

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode show info : ', paloserver.showinfo)
        paloserver.showinfo = True
        print('Mode show info set true : ', paloserver.showinfo)
        ~~~~~~~~~~~~~
        """
        return self.__showinfo

    def _setshowinfo(self, val=True):
        """ internal set of showinfo """
        self.__showinfo = True if val else False

    showinfo = property(_getshowinfo, _setshowinfo)

    def _getshowgputype(self):
        """
        Property showgputype

        Show cubes of type gpu type

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode Type GPU : ', paloserver.showgputype)
        paloserver.showgputype = False
        print('Mode Type GPU set false : ', paloserver.showgputype)
        ~~~~~~~~~~~~~
        """
        return self.__showgputype

    def _setshowgputype(self, val=True):
        """ internal set of showgputype """
        self.__showgputype = True if val else False

    showgputype = property(_getshowgputype, _setshowgputype)

    def _getuseidentifier(self):
        """
        Property useidentifier

        Use identifier in textual representation of the rule

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode rule use identifier : ', paloserver.useidentifier)
        paloserver.useidentifier = True
        print('Mode rule use identifier set true : ', paloserver.useidentifier)
        ~~~~~~~~~~~~~
        """
        return self.__useidentifier

    def _setuseidentifier(self, val=True):
        """ internal set of useidentifier """
        self.__useidentifier = True if val else False

    useidentifier = property(_getuseidentifier, _setuseidentifier)

    def _getshowrule(self):
        """
        Property __showrule

        Show additionnal information rule about the cell value is returned

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode show rule : ', paloserver.showrule)
        paloserver.showrule = True
        print('Mode show rule set true : ', paloserver.showrule)
        ~~~~~~~~~~~~~
        """
        return self.__showrule

    def _setshowrule(self, val=True):
        """ internal set of showrule """
        self.__showrule = True if val else False

    showrule = property(_getshowrule, _setshowrule)

    def _getshowlockinfo(self):
        """
        Property showlockinfo

        Show additionnal information lock about the cell value is returned

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode show lock info : ', paloserver.showlockinfo)
        paloserver.showlockinfo = True
        print('Mode show lock info set true : ', paloserver.showlockinfo)
        ~~~~~~~~~~~~~
        """
        return self.__showlockinfo

    def _setshowlockinfo(self, val=True):
        """ internal set of showlockinfo """
        self.__showlockinfo = True if val else False

    showlockinfo = property(_getshowlockinfo, _setshowlockinfo)

    def _geteventprocessor(self):
        """
        Property eventprocessor

        Setting a new value of cell will possibily call the supervision event \
processor.

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypaloconfig import PypaloConfig

        paloserver = PypaloConfig()

        print('Mode event processor : ', paloserver.eventprocessor)
        paloserver.eventprocessor = False
        print('Mode event processor set false : ', paloserver.eventprocessor)
        ~~~~~~~~~~~~~
        """
        return self.__eventprocessor

    def _seteventprocessor(self, val=True):
        """ internal set of eventprocessor """
        self.__eventprocessor = True if val else False

    eventprocessor = property(_geteventprocessor, _seteventprocessor)

if __name__ == "__main__":
    paloconfig = PypaloConfig()
    print('Server Hostname : ', paloconfig.host)
    paloconfig.host = '192.168.1.100'
    print('New Server Hostname : ', paloconfig.host)
    print('Server Port : ', paloconfig.port)
    paloconfig.port = '7878'
    print('New Server Port : ', paloconfig.port)
    print('User name : ', paloconfig.user)
    paloconfig.user = 'NewUserName'
    print('New User name : ', paloconfig.user)
    print('Password : ', paloconfig.password)
    print('MD5 Password : ', paloconfig.getmd5password())
    paloconfig.password = 'NewPassword'
    print('New password : ', paloconfig.password)
    print('New MD5 Password : ', paloconfig.getmd5password())
    print('Use Proxy : ', paloconfig.useproxy)
    paloconfig.useproxy = True
    print('Enable proxy Result => Use Proxy :', paloconfig.useproxy)
    paloconfig.useproxy = None
    print('Disable proxy Result => Use Proxy :', paloconfig.useproxy)
    print('Proxy URL: ', paloconfig.proxyurl)
    paloconfig.proxyurl = 'http://newproxy:port/'
    print('New proxy URL : ', paloconfig.proxyurl)
    print('Data warehouse Alias Name : ', paloconfig.keydw)
    paloconfig.keydw = 'NewKeyDW'
    print('New data warehouse Alias Name : ', paloconfig.keydw)
    print('Mode normal : ', paloconfig.shownormal)
    paloconfig.shownormal = False
    print('Mode normal set false : ', paloconfig.shownormal)
    print('Mode system : ', paloconfig.showsystem)
    paloconfig.showsystem = True
    print('Mode system set true : ', paloconfig.showsystem)
    print('Mode User Info : ', paloconfig.showuserinfo)
    paloconfig.showuserinfo = True
    print('Mode User Info set true : ', paloconfig.showuserinfo)
    print('Mode Attribute : ', paloconfig.showattribute)
    paloconfig.showattribute = True
    print('Mode Attribute set true : ', paloconfig.showattribute)
    print('Mode Info : ', paloconfig.showinfo)
    paloconfig.showinfo = True
    print('Mode Info set true : ', paloconfig.showinfo)
    print('Mode Type GPU : ', paloconfig.showgputype)
    paloconfig.showgputype = False
    print('Mode Type GPU set False : ', paloconfig.showgputype)
    print('Mode show rule use identifier : ', paloconfig.useidentifier)
    paloconfig.useidentifier = True
    print('Mode show rule use identifier set true : ', paloconfig.useidentifier)
    print('Mode show rule : ', paloconfig.showrule)
    paloconfig.showrule = True
    print('Mode show rule set true : ', paloconfig.showrule)
    print('Mode show lock info : ', paloconfig.showlockinfo)
    paloconfig.showlockinfo = True
    print('Mode show lock info set true : ', paloconfig.showlockinfo)
