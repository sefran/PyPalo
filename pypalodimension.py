"""
File pypalodimension.py
Dimension Request Class for Palo Jedox MOLAP Database
Contains Server Class Palo API

@author Franc SERRES aka sefran
@version 0.5
@licence GPL V3
"""

from pypalo.pypalodatabase import PypaloDatabase

class PypaloDimension(PypaloDatabase):
    """
    Private Constructor Method PypaloDimension('Host', 'Port')
    PypaloDimension = Object pointer of Configuration

    usage:
    ~~~~~~~~~~~~~{.py}
    from pypalo.pypalodimension import PypaloDimension

    palodimension = PypaloDimension('127.0.0.1', '7777')
    if palodimension.login('admin', 'admin'):
        print('Connection Serveur OK')
        if palodimension.getdatabases():
            databases = palodimension.response
            if palodimension.getdimensionsdatabase(databases[0]):
                dimensions = palodimension.response
                "Put your featured code here"
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        if palodimension.logout():
            print('logout OK')
        else:
            print(palodimension.errors)
    else:
        print(palodimension.errors)
    ~~~~~~~~~~~~~
    """
    def __init__(self, host, port):
        """
        host = Ip adress of host server palo like '127.0.0.1'
        port = Number of port host server palo like '7777'
        """
        PypaloDatabase.__init__(self, host, port)

    def cleardimension(self, database, dimension):
        """
        Method cleardimension(database, dimension)

        Clear a dimension.

        database = Dictionary of a database
        dimension = Dictionary of dimension

        Return true if clear dimension request OK
        Property response return dictionary dimension like :
                dictionary : {'id': Identifier of the dimension,
                              'name': Name of the dimension,
                              'number_elements': Number of elements,
                              'maximum_level': Maximum level of the dimension,
                              'maximum_ident': Maximum indent of the dimension,
                              'maximum_depth': Maximum depth of the dimension,
                              'type': Type of dimension (0=normal, 1=system, \
2=attribute),
                              'attributes_dimension': Identifier of the \
attributes dimension of a normal dimension
                              or the identifier of the normal dimension \
associated to a attributes dimension,
                              'attributes_cube': Identifier of the attribute \
cube. (only for normal dimensions),
                              'rights_cube': Identifier of the rights cube \
(only for normal dimensions),
                              'dimension_token': The dimension token of the \
dimension
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodimension import PypaloDimension

        palodimension = PypaloDimension('127.0.0.1', '7777')
        if palodimension.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodimension.getdatabases():
                databases = palodimension.response
                if palodimension.getdimensionsdatabase(databases[0]):
                    dimensions = palodimension.response
                    if palodimension.createdatabase("test"):
                        testdatabase = palodimension.response
                        if palodimension.infodatabase(testdatabase):
                            print('Info test database : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.createdatabase(testdatabase, \
'testdim'):
                            testdimension = palodimension.response
                            print('Creating dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.infodimension(testdatabase, \
testdimension):
                            print('Info dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.cleardimension(testdatabase, \
testdimension):
                            print('Elements testdim cleared : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.destroydatabase(tesdatabase)
                            print('Deleting database \
{0}'.format(tesdatabase["name"]))
                        else:
                            print(palodimension.errors)
                    else:
                        print(palodimension.errors)
                else:
                    print(palodimension.errors)
            else:
                print(palodimension.errors)
            if palodimension.logout():
                print('logout OK')
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'dimension/clear'
        self.param = {'database': database["id"],
                      'dimension': dimension["id"],
                      'sid': self.keydw
                      }
        if self.urlresult:
            listdimension = {"id":None,
                        "name":None,
                        "number_elements":None,
                        "maximum_level":None,
                        "maximum_ident":None,
                        "maximum_depth":None,
                        "type":None,
                        "attributes_dimension":None,
                        "attributes_cube":None,
                        "rights_cube":None,
                        "dimension_token":None,
                        }
            response = self.getdata()
            response = response.split(";")[:-1]
            listdimension["id"] = response[0]
            listdimension["name"] = response[1]
            listdimension["number_elements"] = response[2]
            listdimension["maximum_level"] = response[3]
            listdimension["maximum_ident"] = response[4]
            listdimension["maximum_depth"] = response[5]
            listdimension["type"] = response[6]
            listdimension["attributes_dimension"] = response[7]
            listdimension["attributes_cube"] = response[8]
            listdimension["rights_cube"] = response[9]
            listdimension["dimension_token"] = response[10]
            self.response = listdimension.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Dimension return : '
            self.errors += str(self.getdata())
            return False

    def createdimension(self, database, dimensionname, dimtype=0):
        """
        Method createdimension(database, dimensionname, dimtype)

        Creates a dimension.

        database = Dictionary of a database
        dimensionname = Name of the new dimension
        type = Type of the dimension (0=normal (default), 3=user info)

        Return true if clear dimension request OK
        Property response return dictionary dimension like :
                dictionary : {'id': Identifier of the dimension,
                              'name': Name of the dimension,
                              'number_elements': Number of elements,
                              'maximum_level': Maximum level of the dimension,
                              'maximum_ident': Maximum indent of the dimension,
                              'maximum_depth': Maximum depth of the dimension,
                              'type': Type of dimension (0=normal, 1=system, \
2=attribute),
                              'attributes_dimension': Identifier of the \
attributes dimension of a normal dimension
                              or the identifier of the normal dimension \
associated to a attributes dimension,
                              'attributes_cube': Identifier of the attribute \
cube. (only for normal dimensions),
                              'rights_cube': Identifier of the rights cube \
(only for normal dimensions),
                              'dimension_token': The dimension token of the \
dimension
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodimension import PypaloDimension

        palodimension = PypaloDimension('127.0.0.1', '7777')
        if palodimension.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodimension.getdatabases():
                databases = palodimension.response
                if palodimension.getdimensionsdatabase(databases[0]):
                    dimensions = palodimension.response
                    if palodimension.createdatabase("test"):
                        testdatabase = palodimension.response
                        if palodimension.infodatabase(testdatabase):
                            print('Info test database : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.createdimension(testdatabase, \
'testdim'):
                            testdimension = palodimension.response
                            print('Creating dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.infodimension(testdatabase, \
testdimension):
                            print('Info dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.destroydatabase(tesdatabase)
                            print('Deleting database \
{0}'.format(tesdatabase["name"]))
                        else:
                            print(palodimension.errors)
                    else:
                        print(palodimension.errors)
                else:
                    print(palodimension.errors)
            else:
                print(palodimension.errors)
            if palodimension.logout():
                print('logout OK')
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'dimension/create'
        self.param = {'database': database["id"],
                      'new_name': dimensionname,
                      'type': str(dimtype),
                      'sid': self.keydw
                      }
        if self.urlresult:
            listdimension = {"id":None,
                        "name":None,
                        "number_elements":None,
                        "maximum_level":None,
                        "maximum_ident":None,
                        "maximum_depth":None,
                        "type":None,
                        "attributes_dimension":None,
                        "attributes_cube":None,
                        "rights_cube":None,
                        "dimension_token":None,
                        }
            response = self.getdata()
            response = response.split(";")[:-1]
            listdimension["id"] = response[0]
            listdimension["name"] = response[1]
            listdimension["number_elements"] = response[2]
            listdimension["maximum_level"] = response[3]
            listdimension["maximum_ident"] = response[4]
            listdimension["maximum_depth"] = response[5]
            listdimension["type"] = response[6]
            listdimension["attributes_dimension"] = response[7]
            listdimension["attributes_cube"] = response[8]
            listdimension["rights_cube"] = response[9]
            listdimension["dimension_token"] = response[10]
            self.response = listdimension.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Dimension return : '
            self.errors += str(self.getdata())
            return False

    def getcubesdimension(self, database, dimension):
        """
        Method getcubesdimension(database, dimension)

        Return the list of cubes using a dimension.

        database = Dictionary of a database
        dimension = Dictionary of dimension
        Set properties options :
        shownormal = Show cubes of type normal
        showsystem = Show cubes of type system
        showattribute = Show cubes of type attribute
        showinfo = Show cubes of the type user info
        showgputype = Show cubes of the type gpu type

        Return true if get list of cubes using a dimension request OK
        Property response return dictionary cube like :
                dictionary : {'id': Identifier of the cube,
                              'name': Name of the cube,
                              'number_dimensions': Number of dimensions,
                              'dimensions': list of dictionaries dimension,
                              'number_cells': Total number of cells,
                              'number_filled_cells': Number of filled numeric \
base cells plus number of filled string cells,
                              'status': Status of cube (0=unloaded, 1=loaded \
and 2=changed),
                              'type': Type of cube (0=normal, 1=system, \
2=attribute, 3=user info, 4=gpu type),
                              'token': The cube token of the cube
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodimension import PypaloDimension

        palodimension = PypaloDimension('127.0.0.1', '7777')
        if palodimension.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodimension.getdatabases():
                databases = palodimension.response
                if palodimension.getdimensionsdatabase(databases[0]):
                    dimensions = palodimension.response
                    if palodimension.getcubesdimension(palodimensions[0], \
palodimensions[0]):
                        print('Cubes of database {0} dimension {1}'.format( \
palodimensions[0]["name"], palodimensions[0]["name"]), palodimension.response)
                    else:
                        print(palodimension.errors)
                 else:
                    print(palodimension.errors)
            else:
                print(palodimension.errors)
            if palodimension.logout():
                print('logout OK')
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'dimension/cubes'
        self.param = {'database': database["id"],
                      'dimension': dimension["id"],
                      'show_normal': int(self.shownormal),
                      'show_system': int(self.showsystem),
                      'show_attribute': int(self.showattribute),
                      'show_info': int(self.showinfo),
                      'show_gputype': int(self.showgputype),
                      'sid': self.keydw
                      }
        if self.urlresult:
            response = self.getdata()
            listcubes = []
            if response == '':
                self.response = listcubes.copy()
                return True
            listcube = {"id":None,
                        "name":None,
                        "number_dimensions":None,
                        "dimensions":None,
                        "number_cells":None,
                        "number_filled_cells":None,
                        "status":None,
                        "type":None,
                        "token":None,
                       }
            response = response.split('\n')[:-1]
            for cubedimension in response:
                cubedimension = cubedimension.split(';')[:-1]
                listcube["id"] = cubedimension[0]
                listcube["name"] = cubedimension[1]
                listcube["number_dimensions"] = cubedimension[2]
                listdimensionscube = []
                for dimensioncube in cubedimension[3].split(','):
                    self.infodimension(database, {'id': dimensioncube})
                    listdimensionscube.append(self.response)
                listcube["dimensions"] = listdimensionscube.copy()
                listcube["number_cells"] = cubedimension[4]
                listcube["number_filled_cells"] = cubedimension[5]
                listcube["status"] = cubedimension[6]
                listcube["type"] = cubedimension[7]
                listcube["token"] = cubedimension[8]
                listcubes.append(listcube.copy())
            self.response = listcubes.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Dimension return : '
            self.errors += str(self.getdata())
            return False

    def destroydimension(self, database, dimension):
        """
        Method destroydimension(database, dimension)

        Deletes a dimension.

        database = Dictionary of a database
        dimension = Dictionary of dimension

        Return true if deletes a dimension request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodimension import PypaloDimension

        palodimension = PypaloDimension('127.0.0.1', '7777')
        if palodimension.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodimension.getdatabases():
                databases = palodimension.response
                if palodimension.getdimensionsdatabase(databases[0]):
                    dimensions = palodimension.response
                    if palodimension.createdatabase("test"):
                        testdatabase = palodimension.response
                        if palodimension.infodatabase(testdatabase):
                            print('Info test database : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.createdimension(testdatabase, \
'testdim'):
                            testdimension = palodimension.response
                            print('Creating dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.infodimension(testdatabase, \
testdimension):
                            print('Info dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.destroydimension(testdatabase, \
testdimension):
                            print('Elements testdim deleted : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                else:
                    print(palodimension.errors)
            else:
                print(palodimension.errors)
            if palodimension.logout():
                print('logout OK')
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'dimension/destroy'
        self.param = {'database': database["id"],
                      'dimension': dimension["id"],
                      'sid': self.keydw
                      }
        if self.urlresult:
            response = self.getdata()
            response = response.split(";")[:-1]
            self.response = response[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error deletes a dimension return : '
            self.errors += str(self.getdata())
            return False

    def getelementdimension(self, database, dimension, position):
        """
        Method getelementdimension(database, dimension, position)

        Show element at position "n".

        database = Dictionary of a database
        dimension = Dictionary of dimension
        position = Position of element

        Return true if deletes a dimension request OK
        Property response return dictionary element like :
                dictionary : {'id': Identifier of the element,
                              'name': Name of the element,
                              'position': Number of dimension in the database,
                              'level': Level of the element,
                              'indent': Indent of the element,
                              'depth': Depth of the element,
                              'type': Type of element (1=Numeric, 2=String, \
4=Consolidated),
                              'number_parents': Number of parents,
                              'parents': list of parent identifiers,
                              'number_children': Number of children,
                              'children': list of children identifiers,
                              'weights': list of children weight
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodimension import PypaloDimension

        palodimension = PypaloDimension('127.0.0.1', '7777')
        if palodimension.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodimension.getdatabases():
                databases = palodimension.response
                if palodimension.getdimensionsdatabase(databases[0]):
                    dimensions = palodimension.response
                    if palodimension.getelementsdimension(palodimensions[0], \
palodimensions[0]):
                        print('Elements of database {0} dimension {1}'.format( \
palodimensions[0]["name"], palodimensions[0]["name"]), palodimension.response)
                        paloelements = palodimension.response
                    else:
                        print(palodimension.errors)
                    if palodimension.getelementdimension(palodimensions[0], \
palodimensions[0], paloelements[0]["position"]):
                        print('1st element of database {0} dimension \
{1}'.format( palodimensions[0]["name"], palodimensions[0]["name"]), \
palodimension.response)
                    else:
                        print(palodimension.errors)
                 else:
                    print(palodimension.errors)
                else:
                    print(palodimension.errors)
            else:
                print(palodimension.errors)
            if palodimension.logout():
                print('logout OK')
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'dimension/element'
        self.param = {'database': database["id"],
                      'dimension': dimension["id"],
                      'position': position,
                      'sid': self.keydw
                      }
        if self.urlresult:
            listelement = {"id":None,
                        "name":None,
                        "position":None,
                        "level":None,
                        "ident":None,
                        "depth":None,
                        "type":None,
                        "number_parents":None,
                        "parents":None,
                        "number_children":None,
                        "children":None,
                        "weights":None,
                        }
            response = self.getdata()
            response = response.split(";")[:-1]
            listelement["id"] = response[0]
            listelement["name"] = response[1]
            listelement["position"] = response[2]
            listelement["level"] = response[3]
            listelement["ident"] = response[4]
            listelement["depth"] = response[5]
            listelement["type"] = response[6]
            listelement["number_parents"] = response[7]
            listelement["parents"] = response[8].split(',')
            listelement["number_children"] = response[9]
            listelement["children"] = response[10].split(',')
            listelement["weights"] = response[11].split(',')
            self.response = listelement.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Dimension return : '
            self.errors += str(self.getdata())
            return False

    def getelementsdimension(self, database, dimension):
        """
        Method getelementsdimension(database, dimension)

        Show all elements of a dimension.

        database = Dictionary of a database
        dimension = Dictionary of dimension

        Return true if deletes a dimension request OK
        Property response return dictionary element like :
                dictionary : {'id': Identifier of the element,
                              'name': Name of the element,
                              'position': Number of dimension in the database,
                              'level': Level of the element,
                              'indent': Indent of the element,
                              'depth': Depth of the element,
                              'type': Type of element (1=Numeric, 2=String, \
4=Consolidated),
                              'number_parents': Number of parents,
                              'parents': list of parent identifiers,
                              'number_children': Number of children,
                              'children': list of children identifiers,
                              'weights': list of children weight
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodimension import PypaloDimension

        palodimension = PypaloDimension('127.0.0.1', '7777')
        if palodimension.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodimension.getdatabases():
                databases = palodimension.response
                if palodimension.getdimensionsdatabase(databases[0]):
                    dimensions = palodimension.response
                    if palodimension.getelementsdimension(palodimensions[0], \
palodimensions[0]):
                        print('Elements of database {0} dimension {1}'.format( \
palodimensions[0]["name"], palodimensions[0]["name"]), palodimension.response)
                   else:
                        print(palodimension.errors)
                else:
                    print(palodimension.errors)
            else:
                print(palodimension.errors)
            if palodimension.logout():
                print('logout OK')
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'dimension/elements'
        self.param = {'database': database["id"],
                      'dimension': dimension["id"],
                      'sid': self.keydw
                      }
        if self.urlresult:
            response = self.getdata()
            listelements = []
            if response == '':
                self.response = listelements.copy()
                return True
            listelement = {"id":None,
                        "name":None,
                        "position":None,
                        "level":None,
                        "ident":None,
                        "depth":None,
                        "type":None,
                        "number_parents":None,
                        "parents":None,
                        "number_children":None,
                        "children":None,
                        "weights":None,
                        }
            response = response.split('\n')[:-1]
            for element in response:
                element = element.split(';')[:-1]
                listelement["id"] = element[0]
                listelement["name"] = element[1]
                listelement["position"] = element[2]
                listelement["level"] = element[3]
                listelement["ident"] = element[4]
                listelement["depth"] = element[5]
                listelement["type"] = element[6]
                listelement["number_parents"] = element[7]
                listelement["parents"] = element[8].split(',')
                listelement["number_children"] = element[9]
                listelement["children"] = element[10].split(',')
                listelement["weights"] = element[11].split(',')
                listelements.append(listelement.copy())
            self.response = listelements.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Dimension return : '
            self.errors += str(self.getdata())
            return False

    def infodimension(self, database, dimension):
        """
        Method infodimension(database, dimension)

        Returns dimension information.

        database = Dictionary of a database
        dimension = Dictionary of dimension

        Return true if information dimension request OK
        Property response return dictionary dimension like :
                dictionary : {'id': Identifier of the dimension,
                              'name': Name of the dimension,
                              'number_elements': Number of elements,
                              'maximum_level': Maximum level of the dimension,
                              'maximum_ident': Maximum indent of the dimension,
                              'maximum_depth': Maximum depth of the dimension,
                              'type': Type of dimension (0=normal, 1=system, \
2=attribute),
                              'attributes_dimension': Identifier of the \
attributes dimension of a normal dimension
                              or the identifier of the normal dimension \
associated to a attributes dimension,
                              'attributes_cube': Identifier of the attribute \
cube. (only for normal dimensions),
                              'rights_cube': Identifier of the rights cube \
(only for normal dimensions),
                              'dimension_token': The dimension token of the \
dimension
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodimension import PypaloDimension

        palodimension = PypaloDimension('127.0.0.1', '7777')
        if palodimension.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodimension.getdatabases():
                databases = palodimension.response
                if palodimension.getdimensionsdatabase(databases[0]):
                    dimensions = palodimension.response
                    if palodimension.createdatabase("test"):
                        testdatabase = palodimension.response
                        if palodimension.infodatabase(testdatabase):
                            print('Info test database : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.createdimension(testdatabase, \
'testdim'):
                            testdimension = palodimension.response
                            print('Creating dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.infodimension(testdatabase, \
testdimension):
                            print('Info dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.destroydatabase(tesdatabase)
                            print('Deleting database \
{0}'.format(tesdatabase["name"]))
                        else:
                            print(palodimension.errors)
                    else:
                        print(palodimension.errors)
                else:
                    print(palodimension.errors)
            else:
                print(palodimension.errors)
            if palodimension.logout():
                print('logout OK')
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        """
        self.cmd = 'dimension/info'
        self.param = {'database': database["id"],
                      'dimension': dimension["id"],
                      'sid': self.keydw
                      }
        if self.urlresult:
            listdimension = {"id":None,
                        "name":None,
                        "number_elements":None,
                        "maximum_level":None,
                        "maximum_ident":None,
                        "maximum_depth":None,
                        "type":None,
                        "attributes_dimension":None,
                        "attributes_cube":None,
                        "rights_cube":None,
                        "dimension_token":None,
                        }
            response = self.getdata()
            response = response.split(";")[:-1]
            listdimension["id"] = response[0]
            listdimension["name"] = response[1]
            listdimension["number_elements"] = response[2]
            listdimension["maximum_level"] = response[3]
            listdimension["maximum_ident"] = response[4]
            listdimension["maximum_depth"] = response[5]
            listdimension["type"] = response[6]
            listdimension["attributes_dimension"] = response[7]
            listdimension["attributes_cube"] = response[8]
            listdimension["rights_cube"] = response[9]
            listdimension["dimension_token"] = response[10]
            self.response = listdimension.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Dimension return : '
            self.errors += str(self.getdata())
            return False

    def renamedimension(self, database, dimension, dimensionname):
        """
        Method renamedimension()

        Rename a dimension.

        database = Dictionary of a database
        dimensionname = Name of the new dimension
        dimensionname = New name of the dimension

        Return true if rename dimension request OK
        Property response return dictionary dimension like :
                dictionary : {'id': Identifier of the dimension,
                              'name': Name of the dimension,
                              'number_elements': Number of elements,
                              'maximum_level': Maximum level of the dimension,
                              'maximum_ident': Maximum indent of the dimension,
                              'maximum_depth': Maximum depth of the dimension,
                              'type': Type of dimension (0=normal, 1=system, \
2=attribute),
                              'attributes_dimension': Identifier of the \
attributes dimension of a normal dimension
                              or the identifier of the normal dimension \
associated to a attributes dimension,
                              'attributes_cube': Identifier of the attribute \
cube. (only for normal dimensions),
                              'rights_cube': Identifier of the rights cube \
(only for normal dimensions),
                              'dimension_token': The dimension token of the \
dimension
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodimension import PypaloDimension

        palodimension = PypaloDimension('127.0.0.1', '7777')
        if palodimension.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodimension.getdatabases():
                databases = palodimension.response
                if palodimension.getdimensionsdatabase(databases[0]):
                    dimensions = palodimension.response
                    if palodimension.createdatabase("test"):
                        testdatabase = palodimension.response
                        if palodimension.infodatabase(testdatabase):
                            print('Info test database : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.createdimension(testdatabase, \
'testdim'):
                            testdimension = palodimension.response
                            print('Creating dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.infodimension(testdatabase, \
testdimension):
                            print('Info dimension testdim : ', \
palodimension.response)
                        else:
                            print(palodimension.errors)
                        if palodimension.renamedimension(testdatabase, 
testdimension, \
'NewNameDim'):
                            print('Rename dimension testdim : ', \
palodimension.response)
                            testdimension["name"] = \
palodimension.response["name"]
                        else:
                            print(palodimension.errors)
                        if palodimension.destroydatabase(tesdatabase)
                            print('Deleting database \
{0}'.format(tesdatabase["name"]))
                        else:
                            print(palodimension.errors)
                    else:
                        print(palodimension.errors)
                else:
                    print(palodimension.errors)
            else:
                print(palodimension.errors)
            if palodimension.logout():
                print('logout OK')
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'dimension/rename'
        self.param = {'database': database["id"],
                      'dimension': dimension["id"],
                      'new_name': dimensionname,
                      'sid': self.keydw
                      }
        if self.urlresult:
            listdimension = {"id":None,
                        "name":None,
                        "number_elements":None,
                        "maximum_level":None,
                        "maximum_depth":None,
                        "type":None,
                        "attributes_dimension":None,
                        "attributes_cube":None,
                        "rights_cube":None,
                        "dimension_token":None,
                        }
            response = self.getdata()
            response = response.split(";")[:-1]
            listdimension["id"] = response[0]
            listdimension["name"] = response[1]
            listdimension["number_elements"] = response[2]
            listdimension["maximum_level"] = response[3]
            listdimension["maximum_depth"] = response[4]
            listdimension["type"] = response[5]
            listdimension["attributes_dimension"] = response[6]
            listdimension["attributes_cube"] = response[7]
            listdimension["rights_cube"] = response[8]
            listdimension["dimension_token"] = response[9]
            self.response = listdimension.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error Dimension return : '
            self.errors += str(self.getdata())
            return False


################################################################################

if __name__ == "__main__":
    palodimension = PypaloDimension('127.0.0.1', '7777')
    if palodimension.login('admin', 'admin'):
        print('Connection Serveur OK')
        if palodimension.getdatabases():
            palodatabases = palodimension.response
            if palodimension.getdimensionsdatabase(palodatabases[0]):
                palodimensions = palodimension.response
                if palodimension.getcubesdimension(palodatabases[0], \
                                                            palodimensions[0]):
                    print('Cubes of database {0} dimension {1}'.format( \
                        palodatabases[0]["name"], palodimensions[0]["name"]), \
                                                        palodimension.response)
                else:
                    print(palodimension.errors)
                if palodimension.getelementsdimension(palodatabases[0],
                                                            palodimensions[0]):
                    print('Elements of database {0} dimension {1}'.format( \
                        palodatabases[0]["name"], palodimensions[0]["name"]), \
                                                        palodimension.response)
                    paloelements = palodimension.response
                else:
                    print(palodimension.errors)
                if palodimension.getelementdimension(palodatabases[0],
                                palodimensions[0], paloelements[0]["position"]):
                    print('1st element of database {0} dimension {1}'.format( \
                        palodatabases[0]["name"], palodimensions[0]["name"]), \
                                                        palodimension.response)
                else:
                    print(palodimension.errors)
                if palodimension.createdatabase("test"):
                    print('Database test created')
                    testdatabase = palodimension.response
                    if palodimension.infodatabase(testdatabase):
                        print('Info test database : ', palodimension.response)
                    else:
                        print(palodimension.errors)
                    if palodimension.createdimension(testdatabase, 'testdim'):
                        testdimension = palodimension.response
                        print('Creating dimension testdim : ', \
                                                        palodimension.response)
                    else:
                        print(palodimension.errors)
                    if palodimension.infodimension(testdatabase, testdimension):
                        print('Info dimension testdim : ', \
                                                        palodimension.response)
                    else:
                        print(palodimension.errors)
                    if palodimension.renamedimension(testdatabase, 
testdimension, \
                                                                'NewNameDim'):
                        print('Rename dimension testdim : ', \
                                                        palodimension.response)
                        testdimension["name"] = palodimension.response["name"]
                    else:
                        print(palodimension.errors)
                    if palodimension.cleardimension(testdatabase, \
                                                                testdimension):
                        print('Elements testdim cleared : ', \
                                                        palodimension.response)
                    else:
                        print(palodimension.errors)
                    if palodimension.destroydimension(testdatabase, \
                                                                testdimension):
                        print('Dimension testdim deleted : ', \
                                                        palodimension.response)
                    else:
                        print(palodimension.errors)
                    if palodimension.destroydatabase(testdatabase):
                        print('Database test deleted : ', \
                                                        palodimension.response)
                    else:
                        print(palodimension.errors)
                else:
                    print(palodimension.errors)
            else:
                print(palodimension.errors)
        else:
            print(palodimension.errors)
        if palodimension.logout():
            print('logout OK')
        else:
            print(palodimension.errors)
    else:
        print(palodimension.errors)
