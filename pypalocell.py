from pypalo.pypalocube import PypaloCube

"""
File pypalocell.py
Cell Request Class for Palo Jedox MOLAP Database
Contains Cell Class Palo API

@author Franc SERRES aka sefran
@version 0.5
@licence GPL V3
"""
class PypaloCell(PypaloCube):
    """
    Private Constructor Method PypaloCell('Host', 'Port')
    PypaloCell = Object pointer of Configuration

    usage:
    ~~~~~~~~~~~~~{.py}
    from pypalo.pypalocell import PypaloCell

    palocell = PypaloCell('127.0.0.1', '7777')
    if palocell.login('admin', 'admin'):
        print('Connection Serveur OK')
        if palocell.getdatabases():
            databases = palocell.response
            if palocell.getdimensionsdatabase(databases[0]):
                dimensions = palocell.response
                "Put your featured code here"
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        if palocell.logout():
            print('logout OK')
        else:
            print(palocell.errors)
    else:
        print(palocell.errors)
    ~~~~~~~~~~~~~
    """
    def __init__(self, host, port):
        """
        Parameters :
        host = Ip adress of host server palo like '127.0.0.1'
        port = Number of port host server palo like '7777'
        """
        PypaloCube.__init__(self, host, port)
        self.__userules = False
        self.__baseonly = False
        self.__skipempty = True

    def _getuserules(self):
        """
        Property userules

        userules = Export rule based cell values

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell()

        print('Export use rules : ', palocell.userules)
        palocell.userules = False
        print('Export use rules : ', palocell.userules)
        ~~~~~~~~~~~~~
        """
        return self.__userules

    def _setuserules(self, val=True):
        """ internal set of userules """
        self.__userules = val if val else self.__userules

    userules = property(_getuserules, _setuserules)

    def _getbaseonly(self):
        """
        Property baseonly

        baseonly = Export only base cells

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell()

        print('Export base cell rules : ', palocell.baseonly)
        palocell.baseonly = False
        print('Export base cell rules : ', palocell.baseonly)
        ~~~~~~~~~~~~~
        """
        return self.__baseonly

    def _setbaseonly(self, val=True):
        """ internal set of baseonly """
        self.__baseonly = val if val else self.__baseonly

    baseonly = property(_getbaseonly, _setbaseonly)

    def _getskipempty(self):
        """
        Property skipempty

        skipempty = Export skip empty cell values

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell()

        print('Export skip empty cell : ', palocell.skipempty)
        palocell.skipempty = False
        print('Export skip empty cell : ', palocell.skipempty)
        ~~~~~~~~~~~~~
        """
        return self.__skipempty

    def _setskipempty(self, val=True):
        """ internal set of skipempty """
        self.__skipempty = val if val else self.__skipempty

    skipempty = property(_getskipempty, _setskipempty)

    def getareavaluecell(self, databasecellarea, cubearea, areacells):
        """
        Method getareavaluecell()

        Shows values of area cube cells

        databasecellarea = Dictionary of a database
        cubearea = Dictionary of a cube
        areacells = List of list of dictionary elements or dictionary element\
 of dimension like :
        listcubeelementdimension[
                listelementdimension0[
                                    dictionaryelement{...
                                                    'id': Identifiant element
                                                     }
                                     ],
                dictionaryelementdimension1{...
                                            'id': Identifiant element
                                           }
                                ]

        Parameters properties :
        showrule = If true, then additional information about the rule cell \
value is returned.
                    In case the value originates from an enterprise rule.
        showlockinfo = If true, then additional information about the cell \
lock is returned.

        Return true if get value cube area request OK
        Property response return list of dictionary value like :
            listofvalue[
                dictionaryvalue : {'type': Type of the value (1=numeric, \
2=string),
                                   'exists': 1 if at least base cell for path \
exists,
                                   'value': Value of the cell,
                                   'path': List of element identifier (path of \
cube cell),
                                   'rule': If showrule, identifier of the \
rule, this cell values originates from or empty,
                                   'lock_info': If showlockinfo, Lock info \
(0=cell is not locked, 1=cell is locked by user
                                        wich sent request, 2=cell is locked by \
another user)
                             }
                        ]

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell('127.0.0.1', '7777')
        if palocell.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocell.getdatabases():
                databases = palocell.response
                if palocell.getdimensionsdatabase(databases[0]):
                    dimensions = palocell.response
                    if palocell.createdatabase("test"):
                        testdatabase = palocell.response
                        if palocell.infodatabase(testdatabase):
                            print('Info test database : ', \
palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocell.response)
                            dimension0 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocell.response)
                            dimension1 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocell.response)
                            dimension2 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocell.response)
                            testpalocube = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocell['name']), palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocell.errors)
                        if palocell.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                else:
                    print(palocell.errors)
            else:
                print(palocell.errors)
            if palocell.logout():
                print('logout OK')
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cell/area'
        if len(areacells) != int(cubearea['number_dimensions']):
            self.response = None
            self.errors += 'Palo Error get values area of cube return : '
            self.errors += 'Number of elements list parameters area not \
égual number of dimension cube'
            return False
        area = ''
        for areaelement in areacells:
            if type(areaelement) is list:
                for elarea in areaelement:
                    area += elarea['id'] + ':'
                area = area[:-1] + ','
            else:
                area += areaelement['id'] + ','
        area = area[:-1]
        self.param = {'database': databasecellarea['id'],
                      'cube': cubearea['id'],
                      'area': area,
                      'sid': self.keydw
                     }
        if self.showrule == True:
            self.param.update({'show_rule': int(self.showrule)})
        if self.showlockinfo == True:
            self.param.update({'show_lock_info': int(self.showlockinfo)})
        if self.urlresult:
            responsegetvalueareacell = self.getdata()
            areacellvalue = {'type':None,
                             'exists':None,
                             'value':None,
                             'path':None
                            }
            if self.showrule == True:
                areacellvalue.update({'rule':None})
            if self.showlockinfo == True:
                areacellvalue.update({'lock_info':None})
            responsegetvalueareacell = responsegetvalueareacell.split('\n')[:-1]
            listareacellvalue = []
            for responsevaluecell in responsegetvalueareacell:
                responsevaluecell = responsevaluecell.split(';')[:-1]
                areacellvalue['type'] = responsevaluecell[0]
                areacellvalue['exists'] = responsevaluecell[1]
                areacellvalue['value'] = responsevaluecell[2]
                areacellvalue['path'] = responsevaluecell[3].split(',')
                if self.showrule == True:
                    areacellvalue['rule'] = responsevaluecell[4]
                    if self.showlockinfo == True:
                        areacellvalue['lock_info'] = responsevaluecell[5]
                else:
                    if self.showlockinfo == True:
                        areacellvalue['lock_info'] = responsevaluecell[4]
                listareacellvalue.append(areacellvalue.copy())
            self.response = listareacellvalue.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error get area value cube return : '
            self.errors += str(self.getdata())
            return False

    def copyvaluecell(self, databasecellcopy, cubecellcopy, pathorigin,
                                            pathdestination, valuecopy=None):
        """
        Method copyvaluecell()

        Copies a cell path to an other cell path

        databasecellcopy = Dictionary of a database
        cubecellcopy = Dictionary of a cube
        pathorigin = List of path element identifier like :
                listpathorigincell['id Identifiant element1', 'id Identifiant \
element2', ...]
        pathdestination = List of path element identifier like :
                listpathdestinationcell['id Identifiant element1', 'id \
Identifiant element2', ...]
        valuecopy = The numeric value of the target cube cell (optional).

        Return true if copy value cell request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell('127.0.0.1', '7777')
        if palocell.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocell.getdatabases():
                databases = palocell.response
                if palocell.getdimensionsdatabase(databases[0]):
                    dimensions = palocell.response
                    if palocell.createdatabase("test"):
                        testdatabase = palocell.response
                        if palocell.infodatabase(testdatabase):
                            print('Info test database : ', \
palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocell.response)
                            dimension0 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocell.response)
                            dimension1 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocell.response)
                            dimension2 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocell.response)
                            testpalocube = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocell['name']), palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocell.errors)
                        if palocell.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                else:
                    print(palocell.errors)
            else:
                print(palocell.errors)
            if palocell.logout():
                print('logout OK')
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        ~~~~~~~~~~~~~
        """
        if len(pathorigin) != int(cubecellcopy['number_dimensions']):
            self.response = None
            self.errors = 'Palo Error get value cell cube return : '
            self.errors += 'Number of elements list parameters pathorigin \
area not égual number of dimension cube'
            return False
        if len(pathdestination) != int(cubecellcopy['number_dimensions']):
            self.response = None
            self.errors = 'Palo Error get value cell cube return : '
            self.errors += 'Number of elements list parameters pathdestination \
area not égual number of dimension cube'
            return False
        self.cmd = 'cell/copy'
        self.param = {'database': databasecellcopy['id'],
                      'cube': cubecellcopy['id'],
                      'path': str(pathorigin).replace('\'', '').replace(' ',
                                                                    '')[1:-1],
                      'path_to': str(pathdestination).replace('\'',
                                                    '').replace(' ', '')[1:-1],
                      'sid': self.keydw
                     }
        if valuecopy:
            self.param.update({'value': valuecopy})
        if self.urlresult:
            self.response = self.getdata().split(';')[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error copy value cell cube return : '
            self.errors += str(self.getdata())
            return False

    def drillthroughcell(self, databasedrilltrough, cubedrilltrough, 
pathdrilltrough, modedrilltrough):
        """
        Method drillthroughcell()

        Retrieves detailled data for a cube cell

        databasedrilltrough = Dictionary of a database
        cubedrilltrough = Dictionary of a cube
        pathdrilltrough = List of path element identifier like :
                listpathdrilltrough['id Identifiant element1', 'id Identifiant \
element2', ...]
        modedrilltrough = 1=History, 2=Details, ..., n=User defined

        Return true if copy value cell request OK
        Property response return list of result set

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell('127.0.0.1', '7777')
        if palocell.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocell.getdatabases():
                databases = palocell.response
                if palocell.getdimensionsdatabase(databases[0]):
                    dimensions = palocell.response
                    if palocell.createdatabase("test"):
                        testdatabase = palocell.response
                        if palocell.infodatabase(testdatabase):
                            print('Info test database : ', \
palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocell.response)
                            dimension0 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocell.response)
                            dimension1 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocell.response)
                            dimension2 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocell.response)
                            testpalocube = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocell['name']), palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocell.errors)
                        if palocell.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                else:
                    print(palocell.errors)
            else:
                print(palocell.errors)
            if palocell.logout():
                print('logout OK')
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        ~~~~~~~~~~~~~
        """
        if len(pathdrilltrough) != int(cubedrilltrough['number_dimensions']):
            self.response = None
            self.errors = 'Palo Error get value cell cube return : '
            self.errors += 'Number of elements list parameters pathdrilltrough \
area not égual number of dimension cube'
            return False
        self.cmd = 'cell/drillthrough'
        self.param = {'database': databasedrilltrough['id'],
                      'cube': cubedrilltrough['id'],
                      'path': str(pathdrilltrough).replace('\'', '').replace(
                                                                ' ', '')[1:-1],
                      'mode': modedrilltrough,
                      'sid': self.keydw
                     }
        if self.urlresult:
            self.response = self.getdata().split(',')
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error drillthrough cell cube return : '
            self.errors += str(self.getdata())
            return False

    def getexportcell(self, databaseexportcell, cubeexportcell,
            pathexportcell=None, areaexportcell=None, conditionexportcell=None,
            typeexportcell=None, blocksize=None):
        """
        Method getexportcell()

        Exports value of cube cells

        databaseexportcell = Dictionary of a database
        cubeexportcell = Dictionary of a cube
        pathexportcell = List of path element identifier like :
                listpathcubeelement['id Identifiant element1', 'id Identifiant \
element2', ...]
        areaexportcell = List of list
        conditionexportcell = Condition on the value numeric or string cells \
(default is no condition).
                              Acondition strat with >, >=, <, <=, ==, or != \
and is followed by a double or string.
                              Two condition can be combined by and, or, xor.
        typeexportcell = Type of exported cells. 0=numeric and string, 1=only \
numeric,
                         2=only string (default is 0)
        blocksize = Maximal number of cells to export (default is 1000)

        Parameters properties :
        userules = If true, then export rule based cell values.
        baseonly = If true, then export only base cells.
        skipempty = If false, then export empty cells as well.

        Return true if get export cell cube request OK
        Property response return dictionary export like :
                dictionary : {'type': Type of the value (1=numeric, 2=string),
                              'exists': 1 if at least base cell for path exists,
                              'value': Value of the cell,
                              'rule': If showrule, identifier of the rule, this 
cell values originates from or empty,
                              'lock_info': If showlockinfo, Lock info (0=cell \
is not locked, 1=cell is locked by user
                                        wich sent request, 2=cell is locked by \
another user)
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell('127.0.0.1', '7777')
        if palocell.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocell.getdatabases():
                databases = palocell.response
                if palocell.getdimensionsdatabase(databases[0]):
                    dimensions = palocell.response
                    if palocell.createdatabase("test"):
                        testdatabase = palocell.response
                        if palocell.infodatabase(testdatabase):
                            print('Info test database : ', \
palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocell.response)
                            dimension0 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocell.response)
                            dimension1 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocell.response)
                            dimension2 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocell.response)
                            testpalocube = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocell['name']), palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocell.errors)
                        if palocell.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                else:
                    print(palocell.errors)
            else:
                print(palocell.errors)
            if palocell.logout():
                print('logout OK')
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'cell/export'
        self.param = {'database': databaseexportcell['id'],
                      'cube': cubeexportcell['id'],
                      'sid': self.keydw
                     }
        if pathexportcell:
            self.param.update({'path': str(pathexportcell).replace('\'',
                                                '').replace(' ', '')[1:-1]})
        if areaexportcell:
            area = ''
            for areaelement in areaexportcell:
                if type(areaelement) is list:
                    for elarea in areaelement:
                        area += elarea['id'] + ':'
                    area = area[:-1] + ','
                else:
                    area += areaelement['id'] + ','
            area = area[:-1]
            self.param.update({'area': str(area)})
        if conditionexportcell:
            self.param.update({'condition': conditionexportcell})
        if blocksize:
            self.param.update({'blocksize': blocksize})
        if typeexportcell:
            self.param.update({'type': typeexportcell})
        if self.userules == True:
            self.param.update({'use_rules': '1'})
        if self.baseonly == True:
            self.param.update({'base_only': '1'})
        if self.skipempty == False:
            self.param.update({'skip_empty': '0'})
        if self.urlresult:
            responsegetexportareacell = self.getdata()
            cellexport = {'type':None,
                          'exists':None,
                          'value':None,
                          'path':None
                        }
            listcellsexport = []
            responsegetexportareacell = \
                                    responsegetexportareacell.split('\n')[:-1]
            for elcellexport in responsegetexportareacell[:-1]:
                elcellexport = elcellexport.split(';')[:-1]
                cellexport['type'] = elcellexport[0]
                cellexport['exists'] = elcellexport[1]
                cellexport['value'] = elcellexport[2]
                cellexport['path'] = elcellexport[3].split(',')
                listcellsexport.append(cellexport.copy())
            self.response = listcellsexport.copy()
            self.errors = None
            return responsegetexportareacell[-1].split(';')[:-1]
        else:
            self.response = None
            self.errors += 'Palo Error get export cell cube return : '
            self.errors += str(self.getdata())
            return False

    def goalseekvaluecell(self, databasegoalseek, cubegoalseek, pathgoalseek,
                                                            valuegoalseek=None):
        """
        Method goalseekvaluecell()

        Put value into cell and calculates values for suister cells in order \
to parents remain unchanged.

        databasegoalseek = Dictionary of a database
        cubegoalseek = Dictionary of a cube
        pathgoalseek = List of path element identifier like :
                listpathcubeelement['id Identifiant element1', 'id Identifiant \
element2', ...]
        valuegoalseek = The numeric value of the target cube cell (optional)

        Return true if get value cube area request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell('127.0.0.1', '7777')
        if palocell.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocell.getdatabases():
                databases = palocell.response
                if palocell.getdimensionsdatabase(databases[0]):
                    dimensions = palocell.response
                    if palocell.createdatabase("test"):
                        testdatabase = palocell.response
                        if palocell.infodatabase(testdatabase):
                            print('Info test database : ', \
palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocell.response)
                            dimension0 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocell.response)
                            dimension1 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocell.response)
                            dimension2 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocell.response)
                            testpalocube = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocell['name']), palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocell.errors)
                        if palocell.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                else:
                    print(palocell.errors)
            else:
                print(palocell.errors)
            if palocell.logout():
                print('logout OK')
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        ~~~~~~~~~~~~~
        """
        if len(pathgoalseek) != int(cubegoalseek['number_dimensions']):
            self.response = None
            self.errors = 'Palo Error set goalseek value cube return : '
            self.errors += 'Number of elements list parameters area not \
égual number of dimension cube'
            return False
        self.cmd = 'cell/goalseek'
        self.param = {'database': databasegoalseek['id'],
                      'cube': cubegoalseek['id'],
                      'path': str(pathgoalseek).replace('\'', '').replace(' ',
                                                                    '')[1:-1],
                      'sid': self.keydw
                     }
        if valuegoalseek:
            self.param.update({'value': valuegoalseek})
        if self.urlresult:
            self.response = self.getdata().split(';')[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error set goalseek value cube return : '
            self.errors += str(self.getdata())
            return False

    def setvaluecell(self, databasesetvalue, cubereplacecell, pathsetvalue,
                        cellvaluereplace, splashsetvalue, addsetvalue=None):
        """
        Method setvaluecell()

        Sets or changes the value of cube cell

        databasesetvalue = Dictionary of a database
        cubereplacecell = Dictionary of a cube
        pathsetvalue = List of path element identifier like :
                listpathcubeelement['id Identifiant element1', 'id Identifiant \
element2', ...]
        cellvaluereplace = The numeric or string value of the target cube cell.
        splashsetvalue = Optional splash mode for setting values if list path
                         elements contains consolidated elements
                        (0=no splashing, 1=default, 2=add, 3=set)
        addsetvalue = Optional if true then a numeric value given is added to
                      the existing value or set if no value currently exists. 
                      Setting true requires splashsetvalue mode 0,1 or 2 

        Parameters properties :
        eventprocessor = Setting a new value will possibly call the \
supervision event processor

        Return true if set value cube request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell('127.0.0.1', '7777')
        if palocell.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocell.getdatabases():
                databases = palocell.response
                if palocell.getdimensionsdatabase(databases[0]):
                    dimensions = palocell.response
                    if palocell.createdatabase("test"):
                        testdatabase = palocell.response
                        if palocell.infodatabase(testdatabase):
                            print('Info test database : ', \
palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocell.response)
                            dimension0 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocell.response)
                            dimension1 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocell.response)
                            dimension2 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocell.response)
                            testpalocube = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocell['name']), palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocell.errors)
                        if palocell.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                else:
                    print(palocell.errors)
            else:
                print(palocell.errors)
            if palocell.logout():
                print('logout OK')
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        ~~~~~~~~~~~~~
        """
        if len(pathsetvalue) != int(cubereplacecell['number_dimensions']):
            self.response = None
            self.errors = 'Palo Error set value of cube cell return : '
            self.errors += 'Number of elements list parameters area not \
égual number of dimension cube'
            return False
        self.cmd = 'cell/replace'
        self.param = {'database': databasesetvalue['id'],
                      'cube': cubereplacecell['id'],
                      'path': str(pathsetvalue).replace('\'', '').replace(\
                                                                ' ', '')[1:-1],
                      'value': cellvaluereplace,
                      'splash': splashsetvalue,
                      'sid': self.keydw
                     }
        if self.eventprocessor == False:
            self.param.update({'event_processor': int(self.eventprocessor)})
        if addsetvalue == True and splashsetvalue != '3':
            self.param.update({'add': '1'})
        if self.urlresult:
            self.response = self.getdata().split(';')[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error set value of cube cell return : '
            self.errors += str(self.getdata())
            return False

    def setbulkvaluecell(self, databasereplacebulk, cubereplacebulk,
    pathreplacebulk, valuereplacebulk, splashreplacebulk, addreplacebulk=None):
        """
        Method setbulkvaluecell()

        Sets or changes the value of cube cell

        databasereplacebulk = Dictionary of a database
        cubereplacebulk = Dictionary of a cube
        pathreplacebulk = List of list path element identifier like :
            listoflistpathcubeelement[
                                listpathcubeelement['id Identifiant element1',
                                                'id Identifiant element2', ...
                                                   ],
                                ...
                                     ]
        valuereplacebulk = List of the numeric or string value of the targets \
cube cells.
        splashreplacebulk = Optional splash mode for setting values if list path
                         elements contains consolidated elements
                        (0=no splashing, 1=default, 2=add, 3=set)
        addreplacebulk = Optional if true then a numeric value given is added to
                      the existing value or set if no value currently exists. 
                      Setting true requires splashreplacebulk mode 0,1 or 2 

        Parameters properties :
        eventprocessor = Setting a new value will possibly call the \
supervision event processor

        Return true if set value cube request OK
        Property response return 1 if OK

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell('127.0.0.1', '7777')
        if palocell.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocell.getdatabases():
                databases = palocell.response
                if palocell.getdimensionsdatabase(databases[0]):
                    dimensions = palocell.response
                    if palocell.createdatabase("test"):
                        testdatabase = palocell.response
                        if palocell.infodatabase(testdatabase):
                            print('Info test database : ', \
palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocell.response)
                            dimension0 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocell.response)
                            dimension1 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocell.response)
                            dimension2 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocell.response)
                            testpalocube = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocell['name']), palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocell.errors)
                        if palocell.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                else:
                    print(palocell.errors)
            else:
                print(palocell.errors)
            if palocell.logout():
                print('logout OK')
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        ~~~~~~~~~~~~~
        """
        for pathelement in pathreplacebulk:
            if len(pathelement) != int(cubereplacebulk['number_dimensions']):
                self.response = None
                self.errors = 'Palo Error set bulk value of cube cell return : '
                self.errors += 'Number of elements list parameters area of '
                self.errors += pathelement
                self.errors += ' not égual number of dimension cube'
                return False
        listpath = ''
        for pathelement in pathreplacebulk:
            if type(pathelement) is list:
                for elpath in pathelement:
                    listpath += elpath + ','
                listpath = listpath[:-1] + ':'
        listpath = listpath[:-1]
        self.cmd = 'cell/replace_bulk'
        self.param = {'database': databasereplacebulk['id'],
                      'cube': cubereplacebulk['id'],
                      'paths': listpath,
                      'values': str(valuereplacebulk).replace('\'',
                                '').replace(' ', '').replace(',', ':')[1:-1],
                      'splash': splashreplacebulk,
                      'sid': self.keydw
                     }
        if self.eventprocessor == False:
            self.param.update({'event_processor': int(self.eventprocessor)})
        if addreplacebulk == True and splashreplacebulk != '3':
            self.param.update({'add': '1'})
        if self.urlresult:
            self.response = self.getdata().split(';')[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error set bulk value of cube cell return : '
            self.errors += str(self.getdata())
            return False

    def getvaluecell(self, databasevaluecell, cubevaluecell, pathvaluecell):
        """
        Method getvaluecell()

        Shows the value of cube cell

        databasevaluecell = Dictionary of a database
        cubevaluecell = Dictionary of a cube
        pathvaluecell = List of path element identifier like :
                listpathcubeelement['id Identifiant element1', 'id Identifiant \
element2', ...]

        Parameters properties :
        showrule = If true, then additional information about the cell value \
is returned.
                    In case the value originates from an enterprise rule.
        showlockinfo = If true, then additional information about the cell \
lock is returned.

        Return true if get value cell cube request OK
        Property response return dictionary value like :
                dictionary : {'type': Type of the value (1=numeric, 2=string),
                              'exists': 1 if at least base cell for path exists,
                              'value': Value of the cell,
                              'rule': If showrule, identifier of the rule, this 
cell values originates from or empty,
                              'lock_info': If showlockinfo, Lock info (0=cell \
is not locked, 1=cell is locked by user
                                        wich sent request, 2=cell is locked by \
another user)
                             }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell('127.0.0.1', '7777')
        if palocell.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocell.getdatabases():
                databases = palocell.response
                if palocell.getdimensionsdatabase(databases[0]):
                    dimensions = palocell.response
                    if palocell.createdatabase("test"):
                        testdatabase = palocell.response
                        if palocell.infodatabase(testdatabase):
                            print('Info test database : ', \
palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocell.response)
                            dimension0 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocell.response)
                            dimension1 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocell.response)
                            dimension2 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocell.response)
                            testpalocube = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocell['name']), palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocell.errors)
                        if palocell.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                else:
                    print(palocell.errors)
            else:
                print(palocell.errors)
            if palocell.logout():
                print('logout OK')
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        ~~~~~~~~~~~~~
        """
        if len(pathvaluecell) != int(cubevaluecell['number_dimensions']):
            self.response = None
            self.errors = 'Palo Error get value cell cube return : '
            self.errors += 'Number of elements list parameters area not \
égual number of dimension cube'
            return False
        self.cmd = 'cell/value'
        self.param = {'database': databasevaluecell['id'],
                      'cube': cubevaluecell['id'],
                      'path': str(pathvaluecell).replace('\'', '').replace(' ',
                                                                    '')[1:-1],
                      'sid': self.keydw
                     }
        if self.showrule == True:
            self.param.update({'show_rule': int(self.showrule)})
        if self.showlockinfo == True:
            self.param.update({'show_lock_info': int(self.showlockinfo)})
        if self.urlresult:
            responsegetvalueareacell = self.getdata()
            cellvalue = {'type':None,
                         'exists':None,
                         'value':None,
                        }
            if self.showrule == True:
                cellvalue.update({'rule':None})
            if self.showlockinfo == True:
                cellvalue.update({'lock_info':None})
            responsegetvalueareacell = responsegetvalueareacell.split(';')[:-1]
            cellvalue['type'] = responsegetvalueareacell[0]
            cellvalue['exists'] = responsegetvalueareacell[1]
            cellvalue['value'] = responsegetvalueareacell[2]
            if self.showrule == True:
                cellvalue['rule'] = responsegetvalueareacell[4]
                if self.showlockinfo == True:
                    cellvalue['lock_info'] = responsegetvalueareacell[5]
            else:
                if self.showlockinfo == True:
                    cellvalue['lock_info'] = responsegetvalueareacell[4]
            self.response = cellvalue.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error get value cell cube return : '
            self.errors += str(self.getdata())
            return False

    def getvaluescell(self, databasevaluescell, cubevaluescell, pathsvaluecell):
        """
        Method getvaluescell()

        Shows the values of cube cells

        databasevaluescell = Dictionary of a database
        cubevaluescell = Dictionary of a cube
        pathsvaluecell = List of list path element identifier like :
            listoflistpathcubeelement[
                                listpathcubeelement['id Identifiant element1',
                                                'id Identifiant element2', ...
                                                   ],
                                ...
                                     ]

        Parameters properties :
        showrule = If true, then additional information about the cell value \
is returned.
                    In case the value originates from an enterprise rule.
        showlockinfo = If true, then additional information about the cell \
lock is returned.

        Return true if get value cell cube request OK
        Property response return list of dictionary value like :
            listcells[
                dictionary : {'type': Type of the value (1=numeric, 2=string),
                              'exists': 1 if at least base cell for path exists,
                              'value': Value of the cell,
                              'rule': If showrule, identifier of the rule, this 
cell values originates from or empty,
                              'lock_info': If showlockinfo, Lock info (0=cell \
is not locked, 1=cell is locked by user
                                        wich sent request, 2=cell is locked by \
another user)
                             },
                    ...
                     ]

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalocell import PypaloCell

        palocell = PypaloCell('127.0.0.1', '7777')
        if palocell.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palocell.getdatabases():
                databases = palocell.response
                if palocell.getdimensionsdatabase(databases[0]):
                    dimensions = palocell.response
                    if palocell.createdatabase("test"):
                        testdatabase = palocell.response
                        if palocell.infodatabase(testdatabase):
                            print('Info test database : ', \
palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim0'):
                            print('Creating dimension dim0 : ', \
palocell.response)
                            dimension0 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim1'):
                            print('Creating dimension dim1 : ', \
palocell.response)
                            dimension1 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createdimension(testdatabase, 'dim2'):
                            print('Creating dimension dim2 : ', \
palocell.response)
                            dimension2 = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.createcube(testdatabase, 'testcube', \
[testdimension1, testdimension2], '0'):
                            print('Cube created : ', palocell.response)
                            testpalocube = palocell.response
                        else:
                            print(palocell.errors)
                        if palocell.clearcube(testdatabase, testpalocube):
                            print('Clear cube : ', palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroycube(testdatabase, testpalocube):
                            print('Destroy cube {0} : \
'.format(testpalocell['name']), palocell.response)
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension0):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension1):
                        else:
                            print(palocell.errors)
                        if palocell.destroydimension(testdatabase, \
dimension2):
                        else:
                            print(palocell.errors)
                        if palocell.destroydatabase(testdatabase)
                                print('Deleting database \
{0}'.format(testdatabase["name"]))
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                else:
                    print(palocell.errors)
            else:
                print(palocell.errors)
            if palocell.logout():
                print('logout OK')
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        ~~~~~~~~~~~~~
        """
        for pathelement in pathsvaluecell:
            if len(pathelement) != int(cubevaluescell['number_dimensions']):
                self.response = None
                self.errors = 'Palo Error show values of cube cells return : '
                self.errors += 'Number of elements list parameters area of '
                self.errors += pathelement
                self.errors += ' not égual number of dimension cube'
                return False
        listpath = ''
        for pathelement in pathsvaluecell:
            if type(pathelement) is list:
                for elpath in pathelement:
                    listpath += elpath + ','
                listpath = listpath[:-1] + ':'
        listpath = listpath[:-1]
        self.cmd = 'cell/values'
        self.param = {'database': databasevaluescell['id'],
                      'cube': cubevaluescell['id'],
                      'paths': listpath,
                      'sid': self.keydw
                     }
        if self.showrule == True:
            self.param.update({'show_rule': int(self.showrule)})
        if self.showlockinfo == True:
            self.param.update({'show_lock_info': int(self.showlockinfo)})
        if self.urlresult:
            responsegetvaluescell = self.getdata()
            cellvalue = {'type':None,
                         'exists':None,
                         'value':None,
                        }
            if self.showrule == True:
                cellvalue.update({'rule':None})
            if self.showlockinfo == True:
                cellvalue.update({'lock_info':None})
            responsegetvaluescell = responsegetvaluescell.split(';\n')[:-1]
            listpath = []
            for responsevaluecell in responsegetvaluescell:
                responsevaluecell = responsevaluecell.split(';')
                cellvalue['type'] = responsevaluecell[0]
                cellvalue['exists'] = responsevaluecell[1]
                cellvalue['value'] = responsevaluecell[2]
                if self.showrule == True:
                    cellvalue['rule'] = responsevaluecell[4]
                    if self.showlockinfo == True:
                        cellvalue['lock_info'] = responsevaluecell[5]
                else:
                    if self.showlockinfo == True:
                        cellvalue['lock_info'] = responsevaluecell[4]
                listpath.append(cellvalue.copy())
            self.response = listpath.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors += 'Palo Error get values cells cube return : '
            self.errors += str(self.getdata())
            return False

################################################################################

if __name__ == "__main__":
    palocell = PypaloCell('127.0.0.1', '7777')
    if palocell.login('admin', 'admin'):
        print('Connection Serveur OK')
        if palocell.getdatabases():
            palodatabases = palocell.response
            if palocell.getdimensionsdatabase(palodatabases[0]):
                palocells = palocell.response
                if palocell.createdatabase("test"):
                    print('Database test created')
                    testdatabase = palocell.response
                    if palocell.createdimension(testdatabase, 'testdim0'):
                        print('Creating dimension testdim0 : ', \
                                                        palocell.response)
                        testdimension0 = palocell.response
                    if palocell.createelementbulk(testdatabase, \
                    testdimension0, ['el0', 'el1', 'el2', 'el3'], '1'):
                        print('el0, el1, el2 and el3 created')
                    else:
                        print(palocell.errors)
                    if palocell.getelementsdimension(testdatabase,
                                                                testdimension0):
                        print('Elements of database {0} and dimension \
{1}'.format(testdatabase["name"], testdimension0["name"]))
                        print(palocell.response)
                        elementsdimension0 = palocell.response
                    else:
                        print(palocell.errors)
                    if palocell.createdimension(testdatabase, 'testdim1'):
                        print('Creating dimension testdim1 : ', \
                                                        palocell.response)
                        testdimension1 = palocell.response
                        if palocell.createelementbulk(testdatabase, \
                        testdimension1, ['a0', 'a1', 'a2', 'a3', 'a4'], '1'):
                            print('a0, a1, a2, a3 and a4 created')
                        else:
                            print(palocell.errors)
                        if palocell.getelementsdimension(testdatabase,
                                                                testdimension1):
                            print('Elements of database {0} and dimension \
{1}'.format(testdatabase["name"], testdimension1["name"]))
                            print(palocell.response)
                            elementsdimension1 = palocell.response
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                    if palocell.createdimension(testdatabase, 'testdim2'):
                        print('Creating dimension testdim2 : ', \
                                                        palocell.response)
                        testdimension2 = palocell.response
                        if palocell.createelementbulk(testdatabase, \
                        testdimension2, ['b0', 'b1', 'b2'], '1'):
                            print('b0, b1 and b2 created')
                        else:
                            print(palocell.errors)
                        if palocell.getelementsdimension(testdatabase,
                                                                testdimension2):
                            print('Elements of database {0} and dimension \
{1}'.format(testdatabase["name"], testdimension2["name"]))
                            print(palocell.response)
                            elementsdimension2 = palocell.response
                        else:
                            print(palocell.errors)
                    else:
                        print(palocell.errors)
                    if palocell.createcube(testdatabase, 'testcube',
                                        [testdimension1, testdimension2], '0'):
                        print('Cube created : ', palocell.response)
                        testpalocube = palocell.response
                    else:
                        print(palocell.errors)
                    if palocell.getareavaluecell(testdatabase, testpalocube,
                        [[{'id': '1'}, {'id': '2'}], {'id': '1'}]):
                        print('\nGet area a1-a2,b1 by id : ',
                                                palocell.response)
                        cellpath = palocell.response[0]['path']
                    else:
                        print(palocell.errors)
                    if palocell.setvaluecell(testdatabase, testpalocube,
                                                    cellpath, '10.007', '3'):
                        print('Set value a1,b1 to 10.007 : ',
                                                            palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.getvaluecell(testdatabase, \
                        testpalocube,cellpath):
                        print('Value of a1,b1 : ', palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.getvaluecell(testdatabase, testpalocube,
                        ['4', '2']):
                        print('Value of cell a4,b2', palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.setvaluecell(testdatabase, testpalocube,
                                                    ['4', '2'], '999', '3'):
                        print('Set a4,b2 value to 999 : ', palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.setbulkvaluecell(testdatabase, testpalocube,
                                            [['0', '0'], ['0','1'], ['0','2']], 
                                            ['1', '2', '3'], '3'):
                        print('Set bulk a0,b0-2 with respectively 1,2,3 value')
                    else:
                        print(palocell.errors)
                    if palocell.getvaluescell(testdatabase, testpalocube,
                                        [['0', '0'], ['0','1'], ['0','2']],):
                        print('Shows a0,b0-2 : ', palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.copyvaluecell(testdatabase, testpalocube,
                                                    ['4', '2'], ['2','2']):
                        print('Set a2,b2 value to a4,b2 : ', palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.drillthroughcell(testdatabase, testpalocube,
                                                    ['1', '1'], '2'):
                        print('Drillthrough history cell a1,b1 : ',
                                                            palocell.response)
                    else:
                        print(palocell.errors)
                    #if palocell.goalseekvaluecell(testdatabase, testpalocube,
                    #                                        cellpath, '999'):
                    #    print('Set value to 999 : ', palocell.response)
                    #else:
                    #    print(palocell.errors)
                    state = palocell.getexportcell(testdatabase, testpalocube, 
                        areaexportcell=[[{'id': '0'}, {'id': '1'}, {'id':'2'},
                        {'id': '3'}, {'id': '4'}], [{'id': '0'}, {'id': '1'},
                        {'id': '2'}]], blocksize='5')
                    result = float(state[0]) / float(state[1]) * 100
                    print('Export progress cells : {0}%'.format(str(result)))
                    print('Get export cells: ', palocell.response)
                    #print('Erreurs : ', palocell.errors)
                    if palocell.getareavaluecell(testdatabase, testpalocube,
                        [[{'id': '0'}, {'id': '1'}, {'id': '2'}, {'id': '3'},
                        {'id': '4'}], [{'id': '0'}, {'id': '1'}, {'id': '2'}]]):
                        print('\nShow all cell cube : ',
                                                palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.destroycube(testdatabase, testpalocube):
                        print('\nDestroy cube {0} : \
'.format(testpalocube['name']), palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.destroydimension(testdatabase, \
                                                           testdimension0):
                        print('Dimension testdim deleted : ', \
                                                        palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.destroydimension(testdatabase, \
                                                            testdimension1):
                        print('Dimension testdim deleted : ', \
                                                        palocell.response)
                    else:
                        print(palocell.errors)
                    if palocell.destroydimension(testdatabase, \
                                                            testdimension2):
                        print('Dimension testdim deleted : ', \
                                                        palocell.response)
                    else:
                       print(palocell.errors)
                    if palocell.destroydatabase(testdatabase):
                        print('Database test deleted : ', \
                                                        palocell.response)
                    else:
                        print(palocell.errors)
                else:
                   print(palocell.errors)
            else:
                print(palocell.errors)
        else:
            print(palocell.errors)
        if palocell.logout():
            print('logout OK')
        else:
            print(palocell.errors)
    else:
        print(palocell.errors)
