"""
File pypalodatabase.py
Server Request Class for Palo Jedox MOLAP Database
Contains Database Request Class Palo API

@author Franc SERRES aka sefran
@version 0.5
@licence GPL V3
"""

from pypalo.pypaloserver import PypaloServer

class PypaloDatabase(PypaloServer):
    """
    Private Constructor Method PypaloDatabase('Host', 'Port')
    PypaloDatabase = Object pointer of Configuration

    usage:
    ~~~~~~~~~~~~~{.py}
    from pypalo.pypalodatabase import PypaloDatabase

    palodatabase = PypaloDatabase('127.0.0.1', '7777')
    if palodatabase.login('admin', 'admin'):
        print('Connection Serveur OK')
        "Put your featured code here"
        if palodatabase.logout():
            print('logout OK')
    else:
        print(palodatabase.errors)
    ~~~~~~~~~~~~~
    """
    def __init__(self, host, port):
        """
        host = Ip adress of host server palo like '127.0.0.1'
        port = Number of port host server palo like '7777'
        """
        PypaloServer.__init__(self, host, port)

    def getcubesdatabase(self, cubedatabase):
        """
        Method getcubesdatabase(database)

        Returns the list of cubes.

        database = list of a database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        Return true if return list of cubes
        Property response return list of dictionary cubes like :
            list : [
                dictionary : {'id': Identifier of the cube,
                              'name': Name of the cube,
                              'number_dimensions': Number of dimensions,
                              'dimensions': List of dimension identifiers,
                              'number_cells': Total number of cells,
                              'number_filled_cells': Number of filled numeric
                                base cells plus number of filled string cells,
                              'status': Status of cube (0=unloaded, 1=loaded,
                                        and 2=changed),
                              'type': Type of cube (0=normal, 1=system,
                                        2=attribute, 3=user info, 4=gpu type),
                              'token': The cube token of the cube
                              }
                    ]

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodatabase import PypaloDatabase

        palodatabase = PypaloDatabase('127.0.0.1', '7777')
        if palodatabase.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodatabase.getdatabases():
                database = palodatabase.response
                if palodatabase.getcubesdatabase(database[0]):
                    print('Get Cubes of test database : ', \
palodatabase.response)
                else:
                    print(palodatabase.errors)
            else
                print(palodatabase.errors)
            if palodatabase.logout():
                print('logout OK')
        else:
            print(palodatabase.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'database/cubes'
        self.param = {'database' : cubedatabase["id"],
                      'sid' : self.keydw,
                      'show_normal' : int(self.shownormal),
                      'show_system' : int(self.showsystem),
                      'show_attribute' : int(self.showattribute),
                      'show_info' : int(self.showinfo),
                      'show_gputype' : int(self.showgputype),
                      }
        if self.urlresult:
            response = self.getdata()
            listcubes = []
            if response == '':
                self.response = listcubes.copy()
                return True
            identifiercubes = {"id":None,
                              "name":None,
                              "number_dimensions":None,
                              "dimensions":None,
                              "number_cells":None,
                              "number_filled_cells":None,
                              "status":None,
                              "type":None,
                              "token":None
                             }
            response = response.split('\n')[:-1]
            for cubedatabase in response:
                cubedatabase = cubedatabase.split(';')
                identifiercubes["id"] = cubedatabase[0]
                identifiercubes["name"] = cubedatabase[1]
                identifiercubes["number_dimensions"] = cubedatabase[2]
                identifiercubes["dimensions"] = cubedatabase[3].split(",")
                identifiercubes["number_cells"] = cubedatabase[4]
                identifiercubes["number_filled_cells"] = cubedatabase[5]
                identifiercubes["status"] = cubedatabase[6]
                identifiercubes["type"] = cubedatabase[7]
                identifiercubes["token"] = cubedatabase[8]
                listcubes.append(identifiercubes.copy())
            self.response = listcubes.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors = 'Palo Error Database return list of cubes : '
            self.errors += self.getdata()
            return False

    def createdatabase(self, databasename, dbtype=0):
        """
        Method createdatabase('databasename', dbtype)

        Creates a database.

        databasename = Name of the new database
        dbtype = Type of database (0=normal (default), 3=user info)

        Return true if database created
        Property response return dictionary of database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodatabase import PypaloDatabase

        palodatabase = PypaloDatabase('127.0.0.1', '7777')
        if palodatabase.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodatabase.createdatabase("test"):
                print('Database test created')
                print(palodatabase.response)
                testdatabase = palodatabase.response
            else:
                print(palodatabase.errors)
            if palodatabase.logout():
                print('logout OK')
        else:
            print(palodatabase.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'database/create'
        self.param = {'new_name' : databasename,
                      'sid' : self.keydw,
                      'type' : dbtype,
                      }
        if self.urlresult:
            identifierdatabase = {"id":None,
                                  "name":None,
                                  "number_dimensions":None,
                                  "number_cubes":None,
                                  "status":None,
                                  "type":None,
                                  "token":None
                                 }
            response = self.getdata()
            response = response.split(';')[:-1]
            identifierdatabase["id"] = response[0]
            identifierdatabase["name"] = response[1]
            identifierdatabase["number_dimensions"] = response[2]
            identifierdatabase["number_cubes"] = response[3]
            identifierdatabase["status"] = response[4]
            identifierdatabase["type"] = response[5]
            identifierdatabase["token"] = response[6]
            self.response = identifierdatabase.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors = self.errors.split(',')[3]
            self.errors += ' Palo Error create a database name : '
            self.errors += databasename
            return False

    def destroydatabase(self, destroydatabase):
        """
        Method destroydatabase(database)

        Deletes a database.

        database = dictionary of a database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        Return true if database destroyed
        Property response return 1 if destroyed

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodatabase import PypaloDatabase

        palodatabase = PypaloDatabase('127.0.0.1', '7777')
        if palodatabase.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodatabase.getdatabases():
                print(palodatabase.response)
            if palodatabase.createdatabase("test"):
                print('Database test created')
                print(palodatabase.response)
                testdatabase = palodatabase.response
                if palodatabase.destroydatabase(testdatabase):
                    print('Database test deleted : ', palodatabase.response)
                else:
                    print(palodatabase.errors)
            else:
                print(palodatabase.errors)
            if palodatabase.getdatabases():
                print(palodatabase.response)
            if palodatabase.logout():
                print('logout OK')
        else:
            print(palodatabase.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'database/destroy'
        self.param = {'database' : destroydatabase["id"],
                      'sid' : self.keydw,
                      }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors = 'Palo Error delete database : '
            self.errors += destroydatabase["name"]
            return False

    def getdimensionsdatabase(self, dimensionsdatabase):
        """
        Method getdimensionsdatabase(database)

        Returns the list of dimensions.

        database = dictionary of a database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        Return true if return list of dimensions
        Property response return list of dictionary dimension like :
            list : [
                dictionary : {'id': Identifier of the dimension,
                              'name': Name of the dimension,
                              'number_elements': Number of elements,
                              'maximum_level': Maximum level of the dimension,
                              'maximum_indent': Maximum indent of the dimension,
                              'maximum_depth': Maximum depth of the dimension,
                              'type': Type of dimension (0=normal, 1=system,
                                                    2=attribute, 3=user info),
                              'attributes_dimension': Identifier of the
                                        attributes dimension of a normal
                                        dimension or the identifier of the
                                        normal dimension associated to a
                                        attributes dimension,
                              'attribites_cube': Identifier of the attributes
                                        cube (only for normal dimensions),
                              'rights_cube': Identifier of the rights cube
                                        (only for normal dimensions),
                              'token': The dimension token of the dimension
                              }
                    ]

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodatabase import PypaloDatabase

        palodatabase = PypaloDatabase('127.0.0.1', '7777')
        if palodatabase.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodatabase.getdatabases():
                database = palodatabase.response
                if palodatabase.getdimensionsdatabase(database[0]):
                    print('Get dimensions of database : ', \
palodatabase.response)
                else:
                    print(palodatabase.errors)
            else
                print(palodatabase.errors)
            if palodatabase.logout():
                print('logout OK')
        else:
            print(palodatabase.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'database/dimensions'
        self.param = {'database' : dimensionsdatabase["id"],
                      'sid' : self.keydw,
                      'show_normal' : int(self.shownormal),
                      'show_system' : int(self.showsystem),
                      'show_attribute' : int(self.showattribute),
                      'show_info' : int(self.showinfo),
                      }
        if self.urlresult:
            response = self.getdata()
            listdimension = []
            if response == '':
                self.response = listdimension.copy()
                return True
            identifierdimensions = {"id":None,
                              "name":None,
                              "number_elements":None,
                              "maximum_level":None,
                              "maximum_indent":None,
                              "maximum_depth":None,
                              "type":None,
                              "attributes_dimension":None,
                              "attribites_cube":None,
                              "rights_cube":None,
                              "token":None
                             }
            response = response.split('\n')[:-1]
            for dimensiondatabase in response:
                dimensiondatabase = dimensiondatabase.split(';')[:-1]
                identifierdimensions["id"] = dimensiondatabase[0]
                identifierdimensions["name"] = dimensiondatabase[1]
                identifierdimensions["number_elements"] = dimensiondatabase[2]
                identifierdimensions["maximum_level"] = dimensiondatabase[3]
                identifierdimensions["maximum_indent"] = dimensiondatabase[4]
                identifierdimensions["maximum_depth"] = dimensiondatabase[5]
                identifierdimensions["type"] = dimensiondatabase[6]
                identifierdimensions["attributes_dimension"] = \
                    dimensiondatabase[7]
                identifierdimensions["attribites_cube"] = dimensiondatabase[8]
                identifierdimensions["rights_cube"] = dimensiondatabase[9]
                identifierdimensions["token"] = dimensiondatabase[10]
                listdimension.append(identifierdimensions.copy())
            self.response = listdimension.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors = 'Palo Error Database return list of cubes : '
            self.errors += self.getdata()
            return False

    def infodatabase(self, infodatabase):
        """
        Method infodatabase(database)

        Returns database information.

        database = dictionary of a database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        Return true if info request database OK
        Property response return dictionary of database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodatabase import PypaloDatabase

        palodatabase = PypaloDatabase('127.0.0.1', '7777')
        if palodatabase.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodatabase.getdatabases():
                database = palodatabase.response
                if palodatabase.infodatabase(database[0]):
                    print('Info database : ', palodatabase.response)
                else:
                    print(palodatabase.errors)
            else:
                print(palodatabase.errors)
            if palodatabase.logout():
                print('logout OK')
        else:
            print(palodatabase.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'database/info'
        self.param = {'database' : infodatabase["id"],
                      'sid' : self.keydw,
                      }
        if self.urlresult:
            identifierdatabase = {"id":None,
                                  "name":None,
                                  "number_dimensions":None,
                                  "number_cubes":None,
                                  "status":None,
                                  "type":None,
                                  "token":None
                                 }
            response = self.getdata()
            response = response.split(';')[:-1]
            identifierdatabase["id"] = response[0]
            identifierdatabase["name"] = response[1]
            identifierdatabase["number_dimensions"] = response[2]
            identifierdatabase["number_cubes"] = response[3]
            identifierdatabase["status"] = response[4]
            identifierdatabase["type"] = response[5]
            identifierdatabase["token"] = response[6]
            self.response = identifierdatabase.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors = self.errors.split(',')[3]
            self.errors += ' Palo Error load info of database name : '
            self.errors += infodatabase["name"]
            return False

    def loaddatabase(self, loaddatabase):
        """
        Method loaddatabase(database)

        Loads a database from disk

        database = dictionary of a database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        Return true if info request load OK
        Property response return 1 if loaded

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodatabase import PypaloDatabase

        palodatabase = PypaloDatabase('127.0.0.1', '7777')
        if palodatabase.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodatabase.createdatabase("test"):
                print('Database test created')
                print(palodatabase.response)
                testdatabase = palodatabase.response
                if palodatabase.savedatabase(testdatabase):
                    print('Database test saved : ', palodatabase.response)
                    if palodatabase.unloaddatabase(testdatabase):
                        print('Database test unloaded : ', \
palodatabase.response)
                    else:
                        print(palodatabase.errors)
                    if palodatabase.loaddatabase(testdatabase):
                        print('Database test loaded : ', palodatabase.response)
                    else:
                        print(palodatabase.errors)
                else:
                    print(palodatabase.errors)
            else:
                print(palodatabase.errors)
            if palodatabase.logout():
                print('logout OK')
        else:
            print(palodatabase.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'database/load'
        self.param = {'database' : loaddatabase["id"],
                      'sid' : self.keydw,
                      }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors = self.errors.split(',')[3]
            self.errors += ' Palo Error load database name : '
            self.errors += loaddatabase["name"]
            return False

    def renamedatabase(self, renamedatabase, databasename):
        """
        Method renamedatabase(database, 'databasename')

        Renames a database.

        database = dictionary of a database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }
        databasename = New name of the database
        dbtype = Type of database (0=normal (default), 3=user info)

        Return true if database created
        Property response return dictionary of database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodatabase import PypaloDatabase

        palodatabase = PypaloDatabase('127.0.0.1', '7777')
        if palodatabase.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodatabase.createdatabase("test"):
                print('Database test created')
                print(palodatabase.response)
                testdatabase = palodatabase.response
                if palodatabase.renamedatabase(testdatabase, "Database_Test"):
                    print('Rename of test database : ', palodatabase.response)
                else:
                    print(palodatabase.errors)
                if palodatabase.infodatabase(testdatabase):
                    print('Info after rename : ', palodatabase.response)
                else:
                    print(palodatabase.errors)
            else:
                print(palodatabase.errors)
            if palodatabase.logout():
                print('logout OK')
        else:
            print(palodatabase.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'database/rename'
        self.param = {'database' : renamedatabase["id"],
                      'new_name' : databasename,
                      'sid' : self.keydw,
                      }
        if self.urlresult:
            identifierdatabase = {"id":None,
                                  "name":None,
                                  "number_dimensions":None,
                                  "number_cubes":None,
                                  "status":None,
                                  "type":None,
                                  "token":None
                                 }
            response = self.getdata()
            response = response.split(';')[:-1]
            identifierdatabase["id"] = response[0]
            identifierdatabase["name"] = response[1]
            identifierdatabase["number_dimensions"] = response[2]
            identifierdatabase["number_cubes"] = response[3]
            identifierdatabase["status"] = response[4]
            identifierdatabase["type"] = response[5]
            identifierdatabase["token"] = response[6]
            self.response = identifierdatabase.copy()
            self.errors = None
            return True
        else:
            self.response = None
            self.errors = self.errors.split(',')[3]
            self.errors += ' Palo Error create a database name : '
            self.errors += databasename
            return False

    def savedatabase(self, savedatabase):
        """
        Method savedatabase(database)

        Saves a database to disk.

        database = dictionary of a database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        Return true if info request save OK
        Property response return 1 if saved

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodatabase import PypaloDatabase

        palodatabase = PypaloDatabase('127.0.0.1', '7777')
        if palodatabase.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodatabase.createdatabase("test"):
                print('Database test created')
                print(palodatabase.response)
                testdatabase = palodatabase.response
                if palodatabase.savedatabase(testdatabase):
                    print('Database test saved : ', palodatabase.response)
                else:
                    print(palodatabase.errors)
            else:
                print(palodatabase.errors)
            if palodatabase.logout():
                print('logout OK')
        else:
            print(palodatabase.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'database/save'
        self.param = {'database' : savedatabase["id"],
                      'sid' : self.keydw,
                      }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors = self.errors.split(',')[3]
            self.errors += ' Palo Error save database name : '
            self.errors += savedatabase["name"]
            return False

    def unloaddatabase(self, unloaddatabase):
        """
        Method unloaddatabase(database)

        Unloads a database from memory.

        database = dictionary of a database like :
            dictionary : {'id': Identifier of the database,
                          'name': Name of the database,
                          'number_dimensions': Number of dimension in the
                                               database,
                          'number_cubes': Number of cubes in the database,
                          'status': Status of database (0 is unloaded, 1 is
                                    loaded and 2 is changed),
                          'type': Type of database (0 is normal, 1 is system
                                  and 3 is user info),
                          'token': The database token of database
                          }

        Return true if info request unloaded OK
        Property response return 1 if unloaded

        usage:
        ~~~~~~~~~~~~~{.py}
        from pypalo.pypalodatabase import PypaloDatabase

        palodatabase = PypaloDatabase('127.0.0.1', '7777')
        if palodatabase.login('admin', 'admin'):
            print('Connection Serveur OK')
            if palodatabase.createdatabase("test"):
                print('Database test created')
                print(palodatabase.response)
                testdatabase = palodatabase.response
                if palodatabase.savedatabase(testdatabase):
                    print('Database test saved : ', palodatabase.response)
                    if palodatabase.unloaddatabase(testdatabase):
                        print('Database test unloaded : ', \
palodatabase.response)
                    else:
                        print(palodatabase.errors)
                    if palodatabase.loaddatabase(testdatabase):
                        print('Database test loaded : ', palodatabase.response)
                    else:
                        print(palodatabase.errors)
                else:
                    print(palodatabase.errors)
            else:
                print(palodatabase.errors)
            if palodatabase.logout():
                print('logout OK')
        else:
            print(palodatabase.errors)
        ~~~~~~~~~~~~~
        """
        self.cmd = 'database/unload'
        self.param = {'database' : unloaddatabase["id"],
                      'sid' : self.keydw,
                      }
        if self.urlresult:
            self.response = self.getdata()[0]
            self.errors = None
            return True
        else:
            self.response = None
            self.errors = self.errors.split(',')[3]
            self.errors += ' Palo Error unload database name : '
            self.errors += unloaddatabase["name"]
            return False


################################################################################

if __name__ == "__main__":
    palodatabase = PypaloDatabase('127.0.0.1', '7777')
    if palodatabase.login('admin', 'admin'):
        print('Connection Serveur OK')
        if palodatabase.getdatabases():
            databasedict = palodatabase.response
            if palodatabase.infodatabase(databasedict[0]):
                print('Info database : ', palodatabase.response)
                if palodatabase.getdimensionsdatabase(databasedict[0]):
                    print('Get dimensions of database :\n', \
                                                        palodatabase.response)
                else:
                    print(palodatabase.errors)
                if palodatabase.getcubesdatabase(databasedict[0]):
                    print('Get Cubes of database :\n', palodatabase.response)
                else:
                    print(palodatabase.errors)
            else:
                print(palodatabase.errors)
        if palodatabase.createdatabase("test"):
            print('Database test created')
            print(palodatabase.response)
            testdatabase = palodatabase.response
            if palodatabase.savedatabase(testdatabase):
                print('Save database :', testdatabase["name"], "OK")
            else:
                print(palodatabase.errors)
            if palodatabase.infodatabase(testdatabase):
                print('Info dimension before unload : ', palodatabase.response)
            else:
                print(palodatabase.errors)
            if palodatabase.unloaddatabase(testdatabase):
                print('Unload Database :', testdatabase["name"], "OK")
            else:
                print(palodatabase.errors)
            if palodatabase.infodatabase(testdatabase):
                print('Info dimension after unload : ', palodatabase.response)
            else:
                print(palodatabase.errors)
            if palodatabase.loaddatabase(testdatabase):
                print('Reload Database :', testdatabase["name"], "OK")
            else:
                print(palodatabase.errors)
            if palodatabase.infodatabase(testdatabase):
                print('Info after reload : ', palodatabase.response)
            else:
                print(palodatabase.errors)
            if palodatabase.renamedatabase(testdatabase, "Database_Test"):
                print('Rename of test database : ', palodatabase.response)
                databasedict = palodatabase.response
            else:
                print(palodatabase.errors)
            if palodatabase.destroydatabase(testdatabase):
                print('Database test deleted : ', palodatabase.response)
            else:
                print(palodatabase.errors)
        else:
            print(palodatabase.errors)
        if palodatabase.logout():
            print('logout OK')
        else:
            print(palodatabase.errors)
    else:
        print(palodatabase.errors)
